/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package shapuc.refvis.custom;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import shapuc.refvis.TreeNode;
import shapuc.refvis.Visitor;
import shapuc.refvis.exceptions.CannotAccessFieldException;
import shapuc.refvis.exceptions.VisitorInvokationException;
import shapuc.refvis.reflectiveVisitor.impl.SingleParameterMethodFinder;

/**
 * The Reflective Visitor uses reflection to traverse a tree.
 * 
 * It provides the method invokeVisit(Object) that can be used be executed on an arbitrary object.
 * The TreeNodeType is an arbitrary type being used for doing the object traversal.
 * 
 * @author Stefan Hanenberg (stefan.hanenberg@gmail.com)
 *
 * @param <TreeNodeType>
 * @param <ReturnType>
 */
public class ListAndTreeNodeReflectiveVisitor<TreeNodeType extends TreeNode, ReturnType> 
	implements Visitor {
	
	/**
	 * Runs a reflective call on this Visitor object, i.e. it calls the visitor.
	 *  Clients must always call invokeVisit instead of visit, otherwise the dispatcher
	 *  does not work.
	 *  
	 * @param treeNode
	 */
	public ReturnType invokeVisit(Object treeNode) {
		Method m = new SingleParameterMethodFinder("visit", this.getClass(), treeNode.getClass()).findMethod();
		try {
			return (ReturnType) m.invoke(this, treeNode);
		} catch (InvocationTargetException ex) {
			if (ex.getTargetException() instanceof RuntimeException)
				throw (RuntimeException) ex.getTargetException();
			throw new RuntimeException(ex.getTargetException());
		} catch (IllegalAccessException ex1) {
			throw new VisitorInvokationException("Cannot invoke method " + m.toString() + " on " + this.getClass() + " with parameter type " + treeNode.getClass().getName());
		} catch (IllegalArgumentException ex1) {
			throw new VisitorInvokationException("Cannot invoke method " + m.toString() + " on " + this.getClass() + " with parameter type " + treeNode.getClass().getName());
		}
	}
	
	public void visitAllChildren(TreeNodeType parent) {
		List children = parent.getChildren();
		for (int i = 0; i < children.size(); i++) {
			this.invokeVisit(children.get(i));
		}
	}	
	

}

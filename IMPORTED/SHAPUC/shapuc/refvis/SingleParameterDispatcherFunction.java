package shapuc.refvis;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import shapuc.refvis.exceptions.VisitorInvokationException;
import shapuc.refvis.reflectiveVisitor.impl.SingleParameterMethodFinder;

public class SingleParameterDispatcherFunction<ReturnType, TreeNodeType> {

	final Object targetObject;
	final String visitMethodName; 
	
	public SingleParameterDispatcherFunction(Object targetObject, String visitMethodName) {
		super();
		this.targetObject = targetObject;
		this.visitMethodName = visitMethodName;
	}

	/**
	 * Dispatches the method call to the most appropriate method using a SingleParameterMethodFiner. 
	 * 
	 * @param treeNode
	 * @return
	 */
	public ReturnType invokeVisit(TreeNodeType treeNode) {
		Method m = new SingleParameterMethodFinder(visitMethodName, targetObject.getClass(), treeNode.getClass()).findMethod();
		try {
			return (ReturnType) m.invoke(targetObject, treeNode);
		} catch (InvocationTargetException ex) {
			if (ex.getTargetException() instanceof RuntimeException)
				throw (RuntimeException) ex.getTargetException();
			throw new RuntimeException(ex.getTargetException());
		} catch (IllegalAccessException ex1) {
			throw new VisitorInvokationException("Cannot invoke method " + m.toString() + " on " + this.getClass() + " with parameter type " + treeNode.getClass().getName());
		} catch (IllegalArgumentException ex1) {
			throw new VisitorInvokationException("Cannot invoke method " + m.toString() + " on " + this.getClass() + " with parameter type " + treeNode.getClass().getName());
		}
	}
}

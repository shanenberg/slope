package shapuc.refvis;

/**
 * This class handles the dispatching of a method based on the actual parameter type 
 * (methods with single parameters)
 */
public class SingleParamDispatcher<ReturnType, ParamType> {

	final SingleParameterDispatcherFunction<ReturnType, ParamType> dispatcher;

	public SingleParamDispatcher(String methodName) {
		dispatcher = new SingleParameterDispatcherFunction<ReturnType, ParamType>(this, methodName);		
	}
	
	
	public ReturnType invoke(ParamType param) {
		return dispatcher.invokeVisit(param);
	}
	
}

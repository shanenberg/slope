package shapuc.graphs.simple;

import shapuc.graphs.Edge;

public class SimpleEdge extends Edge<SimpleNode> {

	public SimpleEdge(SimpleNode source, SimpleNode target) {
		super(source, target);
	}

}

package shapuc.graphs.grapical;

import shapuc.graphs.Node;

public class GraphicalNode extends Node<GraphicalNode>{

	public GraphicalNode(String identifier) {
		super(identifier);
	}

	int x = 0;
	int y = 0;

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public String toString() {
		return super.toString() + "[x=" + x + ", y=" + y + "]";
	}
	
	
	
}

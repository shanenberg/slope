package shapuc.graphs.factories;

import shapuc.graphs.Edge;
import shapuc.graphs.Graph;
import shapuc.graphs.Node;
import shapuc.graphs.grapical.GraphicalNode;

public class ExampleGraphs {

	public <NodeType extends Node<NodeType>, EdgeType extends Edge<NodeType>> Graph<NodeType, EdgeType> createExampleGraph01(
			GraphFactory<NodeType, EdgeType> graphFactory) {

		Graph<NodeType, EdgeType> graph = graphFactory.createGraph();

		graph.addNode("A");
		graph.addNode("B");
		graph.addNode("E");
		graph.addNode("D");
		graph.addNode("F");

		graph.addEdge("A", "B");
		graph.addEdge("B", "E");
		graph.addEdge("B", "A");
		graph.addEdge("E", "D");
		graph.addEdge("D", "E");
		graph.addEdge("D", "A");
		graph.addEdge("A", "F");

		return graph;
	}

	public Graph<GraphicalNode, Edge<GraphicalNode>> createGraphicalGraph() {
		GraphFactoryGraphical graphFactory = new GraphFactoryGraphical();
		Graph<GraphicalNode, Edge<GraphicalNode>> graph = createExampleGraph01(graphFactory);
		
		int positionSetter = 20;
		for (GraphicalNode n : graph.getNodes()) {
			n.setX(positionSetter);
			n.setY(positionSetter);
			positionSetter = positionSetter + 20;
		}
		
		graph.getNode("A").setX(180);
		
		return graph;
	
	}		
}

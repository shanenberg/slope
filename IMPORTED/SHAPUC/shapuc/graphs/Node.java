package shapuc.graphs;

import java.util.ArrayList;

public class Node<NodeType> {
	String identifier;
	
	public Node(String identifier) {
		super();
		this.identifier = identifier;
	}
	
	public String getIdentifier() {
		return identifier;
	}	
	
	public String toString() {
		return "\"" + identifier + "\"";
	}
}

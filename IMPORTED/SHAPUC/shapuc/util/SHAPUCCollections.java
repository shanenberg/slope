/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package shapuc.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SHAPUCCollections {
	public static <T1, T2> List<T1> unsaveToList(T2[] t) {
		ArrayList l = new ArrayList();
		for (int i = 0; i < t.length; i++) {
			l.add(t[i]);
		}
		return l;
	}
	
	public static List<Character> unsaveToList(char[] t) {
		ArrayList<Character> l = new ArrayList<Character>();
		for (int i = 0; i < t.length; i++) {
			l.add(t[i]);
		}
		return l;
	}
	
	public static  void putWithDefaultList(HashMap hashmap, Object key, Object value, Class<? extends List> _defaultListType) {
		if (hashmap.containsKey(key)) {
			((List) hashmap.get(key)).add(value);
		} else {
			List l = (List) Reflection.instantiateClassUnsave(_defaultListType);
			l.add(value);
			HashMap casted = (HashMap) hashmap;
			casted.put(key,  l);
		}
	}

	public static <ArrayType> HashMap<Character, Character> translate2DimArrayIntoHashmap(ArrayType[][] quasiMap) {
		HashMap ret =  new HashMap();
		for (int i = 0; i < quasiMap.length; i++) {
			ret.put(quasiMap[i][0], quasiMap[i][1]); 
		}
		
		return ret;
	}
	
}

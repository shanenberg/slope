/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

package slowscane.lib;

import junit.framework.TestCase;
import slowscane.Tokens;
import slowscane.annotations.Keyword;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.scanners.TokenReader;

/**
 * 
 * @author Stefan Hanenberg (stefan.hanenberg@gmail.com)
 *
 */
public class MiniJava01 extends TestCase {

	public static class MiniJava01Tokens extends Tokens {
		
		@Separator public TokenReader<?> WHITESPACE() {	return lib.WHITESPACE; 	}

		@Keyword public TokenReader<?> CLASS() { return lib.KEYWORD_CLASS; }
		@Keyword public TokenReader<?> VOID() { return lib.KEYWORD_VOID; }
		
		@Token public TokenReader<?> DOT() { return lib.DOT; }
		@Token public TokenReader<?> LAMBDA() { return lib.LAMBDA; }

		@Token public TokenReader<?> OPEN_BRACKET() { return lib.OPEN_BRACKET; }
		@Token public TokenReader<?> OPEN_CURLY_BRACKET() { return lib.OPEN_CURELY_BRACKET; }
		@Token public TokenReader<?> CLOSED_BRACKET() { return lib.CLOSED_BRACKET; }
		@Token public TokenReader<?> CLOSED__CURLY_BRACKET() { return lib.CLOSED_CURLY_BRACKET; }
		@Token public TokenReader<?> SEMICOLON() { return lib.SEMICOLON; }

		@Token public TokenReader<String> JAVA_IDENTIFIER() {	return lib.JAVA_IDENTIFIER; }
		@Token public TokenReader<?> JAVA_INTEGER() {	return lib.JAVA_INTEGER_LITERAL; }
	}

	public void test01() {
		MiniJava01Tokens t = new MiniJava01Tokens();
		t.getTokenTypes();
		t.getSkippedTokenTypes();
	}

}

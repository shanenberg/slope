/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slowscane.scanners;

import slowscane.ScanException;
import slowscane.Token;
import slowscane.TokenType;
import slowscane.streams.PositionStream;
import slowscane.streams.TokenStream;

public abstract class TokenReader<TOKENVALUETYPE> implements TokenParser<TOKENVALUETYPE>{
	
//	protected String elementName;
	protected TokenType tokenType;
	
//	public String getElementName() {
//		return elementName;
//	}

	protected TokenType getTokenType() {
		return tokenType;
	}
	
	/** Note: the TokenReader gets his name from the Tokens object, i.e. the token name
	 * is not part of the tokens definition. 
	 */
//	public void setElementName(String elementName) {
//		this.elementName = elementName;
//	}

	public TokenReader() {
		super();
	}

	public abstract Token<TOKENVALUETYPE> scanNext(PositionStream<?> stream);

	/** 
	 * Creates the token-object.
	 * 
	 * @param stream
	 * @param value
	 * @return
	 */
	public Token<TOKENVALUETYPE> createToken(PositionStream<?> stream, TOKENVALUETYPE value) {
		return  new Token<TOKENVALUETYPE>(getTokenType(), value, stream.currentPosition);
	}
	
//	public Token<TOKENVALUETYPE> createToken(PositionStream<?> stream, TOKENVALUETYPE value) {
//		return  new Token<TOKENVALUETYPE>(getTokenType(), elementName, value, stream.currentPosition);
//	}	
	
	/**
	 * This method is used by the parser, NOT the scanner!!!!!
	 */
	@SuppressWarnings("unchecked")
	public Token<TOKENVALUETYPE> parseNext(TokenStream<?> tokenstream) {
		Token<?> nextToken = tokenstream.next();
		if (!tokenIsOfRightKind(nextToken))
			throw new ScanException(this.getClass().getName() + ": Next token not as expected", tokenstream.stuckExceptionString());
		return (Token<TOKENVALUETYPE>) nextToken;
	}	
	
	/**
	 * Method checks, whether the delivered token is the one expected by the TokenReader.
	 * 
	 * The method is intended to be overridden by special class such as KeywordScanner 
	 * 
	 * @param token
	 * @return
	 */
	protected boolean tokenIsOfRightKind(Token<?> token) {
		return token.getTokenType().equals(getTokenType());
	}
	
}

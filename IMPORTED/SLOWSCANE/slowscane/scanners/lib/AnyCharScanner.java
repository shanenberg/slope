package slowscane.scanners.lib;

import java.util.HashMap;

import shapuc.util.SHAPUCCollections;
import slowscane.Token;
import slowscane.scanners.TokenReader;
import slowscane.streams.PositionStream;

public class AnyCharScanner extends TokenReader<Character> {

	Object escapingChar;
	
	HashMap<Character, Character> escapeTranslations = new HashMap<Character, Character>();
	
	
	public AnyCharScanner() {
		super();
		this.escapingChar = null;
	}	
	
	public AnyCharScanner(Object escapeChar, Object[][] escapedChars) {
		super();
		this.escapingChar = escapeChar;
		escapeTranslations = SHAPUCCollections.translate2DimArrayIntoHashmap(escapedChars);
	}


	public Token<Character> scanNext(PositionStream<?> stream) {
		Character currentChar = (Character) stream.next();
		
		if (currentChar.equals(escapingChar)) {
			Character escapedChar = (Character) stream.next();
			return this.createToken(stream, escapeTranslations.get(escapedChar));
		}
		
		return this.createToken(stream, currentChar);
	}

	

}

/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package sloc.columnTypes;

import java.util.ArrayList;
import java.util.HashMap;

import shapuc.util.Pair;
import sloc.ColumnType;
import sloc.parsing.CSVBodyParserTokens.CSVBodyParser;
import slope.Cmd;
import slope.PC;
import slope.ResultObject;
import slope.SetCmd;

public class MapType extends ColumnType{

	ColumnType keyType;
	ColumnType valueType;
	
	public ColumnType getKeyType() {
		return keyType;
	}

	public void setKeyType(ColumnType keyType) {
		this.keyType = keyType;
	}

	public ColumnType getValueType() {
		return valueType;
	}

	public void setValueType(ColumnType valueType) {
		this.valueType = valueType;
	}

	@Override
	public String getTypeName() {
		System.out.println(keyType);
		System.out.println(valueType);
		return "Map(" + keyType.getTypeName() + ":" + valueType.getTypeName() + ")";
	}

	
	public PC createParserConstructor(final CSVBodyParser parser, final SetCmd<Object> setResult) {
		
		final HashMap ret = new HashMap();
		
		SetCmd<Object> setKey = new SetCmd<Object>() {
			public void set(Object o) {
				ret.put(o, null);
			}
		};	
		
		SetCmd<Pair<Object, Object>> addElement = new SetCmd<Pair<Object, Object>>() {
			public void set(Pair<Object, Object> o) {
				ret.put(o.getLeft(), o.getRight());
			}
		};			
		
		return 
		parser.AND(
			parser.TOKEN('{'), 
			parser.OR(
				    parser.TOKEN('}'),
				    parser.AND(
				    	parser.WITH_OPTIONAL(
				    			createKeyValue(parser, addElement),
				    			parser.NFOLD(
				    				parser.AND(
				    						parser.TOKEN(','),
				    						createKeyValue(parser, addElement)
				    				)
				    			)
				    	),
					    parser.TOKEN('}')
					)
			),
			parser.set(parser.setResult(setResult, ret))
		);
	}

	public PC createKeyValue(final CSVBodyParser parser, final SetCmd<Pair<Object, Object>> setResult) {
		
		final ResultObject<Pair<Object, Object>> keyValueResult = new ResultObject<Pair<Object, Object>>();
		keyValueResult.result = new Pair<Object, Object>();

		SetCmd<Object> setKey = new SetCmd<Object>() {
			public void set(Object value) {
				keyValueResult.result.setLeft(value);
			}
		};
		
		SetCmd<Object> setValue = new SetCmd<Object>() {
			public void set(Object value) {
				keyValueResult.result.setRight(value);
			}
		};

		return
		parser.AND(
				parser.TOKEN('('),
				keyType.createParserConstructor(parser, setKey),
				parser.TOKEN(':'),
				valueType.createParserConstructor(parser, setValue),
				parser.TOKEN(')'),				
				parser.set(parser.setResult(setResult, keyValueResult.result))
		);
	}


}

/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package sloc.columnTypes;

import java.util.ArrayList;

import sloc.ColumnType;
import sloc.parsing.CSVBodyParserTokens.CSVBodyParser;
import slope.Cmd;
import slope.PC;
import slope.SetCmd;

public class ListType extends ColumnType {

	public ColumnType componentType;
	
	public ColumnType getComponentType() {
		return componentType;
	}

	public void setComponentType(ColumnType componentType) {
		this.componentType = componentType;
	}

	@Override
	public String getTypeName() {
		return "List(" + getComponentType().getTypeName() + ")";
	}

	@Override
	public PC createParserConstructor(final CSVBodyParser parser, final SetCmd<Object> setResult) {
		
//		SETObject<TOKENTreeNode<Integer>> setResult = new SETObject<TOKENTreeNode<Integer>>() {
//			public void set(TOKENTreeNode<Integer> value) {
//				Reflection.setFieldUnsave(fieldName, value.getTokenValue(), targetObject);
//			}
//		};
		
		final ArrayList ret = new ArrayList();
		
		Cmd setReturn = new Cmd() {
			public void doCmd() {
				setResult.set(ret);
			}
		};
		
		SetCmd<Object> addElement = new SetCmd<Object>() {
			public void set(Object o) {
				ret.add(o);
			}
		};		
		
		return 
				parser.AND(parser.TOKEN('{'), 
				  parser.OR(
				    parser.TOKEN('}'),
					parser.OR(
					  parser.AND(
					    componentType.createParserConstructor(parser, addElement),
					    parser.AND(
					      parser.NFOLD(
					        parser.AND(
						      parser.TOKEN(','), 
						      componentType.createParserConstructor(parser, addElement))
					        ),
					      parser.TOKEN('}'))),
					  parser.AND(
					    componentType.createParserConstructor(parser, addElement),
					    parser.TOKEN('}')),
					  parser.TOKEN('}')
					)
				  ),
			      parser.set(setReturn)
			);
	}

}

/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope.testing;

import junit.framework.TestCase;
import slope.PC;
import slope.Parser;
import slope.Parsing;
import slope.Rule;
import slope.TreeNodeImpl_1;
import slope.TreeNode_DefaultImpl;
import slope.lang.BIND.BINDTreeNode;
import slowscane.annotations.Token;
import slowscane.lib.MiniJava01;
import slowscane.scanners.KeywordScanner;
import slowscane.scanners.TokenReader;

public class Testing06_SimpleOR extends TestCase {
	
	public static class MiniJavaParser extends MiniJava01.MiniJava01Tokens {

		@Token public TokenReader<?> A() {return new KeywordScanner("A");}
		@Token public TokenReader<?> B() {return new KeywordScanner("B");}
		
		public class MyParser extends Parser<MiniJavaParser> {
			Rule START() { return new Rule() { public PC pc() { return 
					OR(TOKEN("A"), TOKEN("B"));
			}};}
		}
	}
	
	public void test01() {
		MiniJavaParser t = new MiniJavaParser();
		parse("A");
		parse("B");

		try {
			parse("AB");	
			assertTrue("AB should not be accepted here!", false);
		} catch (Exception e) {
		}
	}
	
	public void parse(String s) {
		MiniJavaParser t = new MiniJavaParser();
		Parsing p = new Parsing(t, s);
		TreeNodeImpl_1 tn = (TreeNodeImpl_1) p.parse(t.new MyParser().START());
	}	
}

/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope.testing;

import junit.framework.TestCase;
import slope.PC;
import slope.ParseException;
import slope.Parser;
import slope.Parsing;
import slope.Rule;
import slope.TreeNode;
import slope.TreeNodeImpl_1;
import slope.lang.TOKEN.TokenTreeNode;
import slowscane.lib.Lambda01;

public class Testing extends TestCase {
	
	public class Lambda01Parser extends Lambda01.Lambda01Tokens {

		public class MyParser extends Parser<Lambda01Parser> {
			Rule START() { return new Rule() { public PC pc() { return 
					OR(
						TOKEN(JAVA_IDENTIFIER()), 
						TOKEN(OBRACKET()));
			}};}
		}
	}
	
	
	public void test01() {
		// Parser for IDENTIFIER()|| "("
		Lambda01Parser t = new Lambda01Parser();
		Parsing p = new Parsing(t, "abc");
		TreeNode tn = p.parse(t.new MyParser().START());
		
//		assertEquals("START", ((TreeNodeImpl_1) tn).getTreeNodeName());
		TokenTreeNode tokenTn = (TokenTreeNode) ((TreeNodeImpl_1) ((TreeNodeImpl_1) tn).treeNode).treeNode;
		
//		assertEquals("JAVA_IDENTIFIER", tokenTn.treeNodeName);
		assertEquals("abc", tokenTn.getToken().getValue());

		/* ++++++++++++++++ */
		
		p = new Parsing(t, "(");
		tn = p.parse(t.new MyParser().START());
		
//		assertEquals("START", ((TreeNodeImpl_1) tn).getTreeNodeName());
		tokenTn = (TokenTreeNode) ((TreeNodeImpl_1) ((TreeNodeImpl_1) tn).treeNode).treeNode;
		
//		assertEquals("OBRACKET", tokenTn.treeNodeName);
		assertEquals('(', tokenTn.getToken().getValue());
		
		
		p = new Parsing(t, "aasdasd asd");
		try {
			tn = (TokenTreeNode) p.parse(t.new MyParser().START());
			assertTrue("Exception should be thrown - EOF not reached", false);
		} catch (ParseException ex) {
			assertTrue(ex.scanStringWithError.startsWith("\"aasdasd" ));
		}

	
	}
}

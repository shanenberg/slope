/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope.testing;

import junit.framework.TestCase;
import slope.PC;
import slope.Parser;
import slope.Parsing;
import slope.Rule;
import slope.TreeNodeImpl_1;
import slope.TreeNode_DefaultImpl;
import slope.lang.BIND.BINDTreeNode;
import slowscane.lib.MiniJava01;

public class Testing05_SCOPE extends TestCase {
	
	public static class MiniJavaParser extends MiniJava01.MiniJava01Tokens {

		public static class Result extends TreeNode_DefaultImpl {}
		
		public class MyParser extends Parser<MiniJavaParser> {
			Rule START() { return new Rule() { public PC pc() { return 
					BIND(Result.class,
						TOKEN(JAVA_IDENTIFIER())
					);
			}};}
		}
	}
	
	
	public void test01() {
		MiniJavaParser t = new MiniJavaParser();
		Parsing p = new Parsing(t, "abc");
		TreeNodeImpl_1 tn = (TreeNodeImpl_1) p.parse(t.new MyParser().START());
		assertTrue(tn.treeNode instanceof BINDTreeNode);
		
		
		
//		TokenTreeNode tn2 = ((TokenTreeNode) tn.treeNode);
//		assertEquals("abc", ((TokenTreeNode) tokenTreeNode.get(0)).getToken().getValue());
//	
//		t = new MiniJavaParser();
//		p = new Parsing(t, "asd fda ggg wertwert ");
//		tokenTreeNode = (TreeNode_N) p.parse(t.new MyParser().START());
//		assertEquals("asd", ((TokenTreeNode) tokenTreeNode.get(0)).getToken().getValue());
//		assertEquals("fda", ((TokenTreeNode) tokenTreeNode.get(1)).getToken().getValue());
//		assertEquals("ggg", ((TokenTreeNode) tokenTreeNode.get(2)).getToken().getValue());
//		assertEquals("wertwert", ((TokenTreeNode) tokenTreeNode.get(3)).getToken().getValue());
//	
	}
}

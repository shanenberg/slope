package slope.core.grammar;

import java.util.ArrayList;

import junit.framework.TestCase;
import slope.Parsing;
import slope.ResultObject;
import slope.SetCmd;
import slope.lib.grammar.GrammarParsingWithTokens;
import slope.lib.grammar.GrammarParsingWithTokens.GrammarWithTokensParser;
import slope.lib.grammar.parsetree.Expression;
import slope.lib.grammar.parsetree.Grammar;
import slope.lib.grammar.parsetree.Terminal;
import slope.lib.grammar.parsetree.Token;

public class GrammarParsingWithTokensTest extends TestCase {

	public void test04() {
		Grammar r;
		r = parseGrammar("asd Start: asd Rules: abc-> asd.");
	}
	
	public void test03() {
		Terminal r;
		r = parseTerminal("\"asd\"");
		r = parseTerminal("<ABC>");
	}
	
	public void test02() {
		ArrayList<Token> r;
		r = parseTokenDeclarations(
			"TOKENS:"
			+ "<abc> 'a'|'b'|'c'.");
		assertEquals(1, r.size());

		r = parseTokenDeclarations(
				"TOKENS:"
				+ "<abc> ('a'|'b'|('c')*).");
		assertEquals(1, r.size());

		r = parseTokenDeclarations(
				"TOKENS:"
				+ "<abc> ('a'|'b'|('c')*)."
				+ "<xyz> (('x')*).");
		assertEquals(2, r.size());
		
		r = parseTokenDeclarations(
				"TOKENS:"
				+ "SKIPPED <abc> ('a'|'b'|('c')*)."
				+ "<xyz> (('x')*).");
		assertEquals(2, r.size());		
	}	
	
	public void test01() {
		Token r;
		r = parseTokenDeclaration("<abc> 'a'|'b'|'c'.");
		assertEquals("'a'|'b'|'c'", r.regExString);
		
		r = parseTokenDeclaration("<asd> (  'a'   | 'b'    | 'c')*.");
		assertEquals("('a'|'b'|'c')*", r.regExString);

		r = parseTokenDeclaration("<asd> '\\n'.");
//		assertEquals("'\\n'", r.regExString);
	
	}
	
	private Grammar parseGrammar(String s) {
		GrammarParsingWithTokens tokens = new GrammarParsingWithTokens();
		
		@SuppressWarnings("rawtypes")
		Parsing<GrammarParsingWithTokens> p = new Parsing<GrammarParsingWithTokens>(tokens, s); 
		ResultObject<Grammar> result = new ResultObject<Grammar>();
		
		p.parseWithParseTreeConstruction(tokens.new GrammarWithTokensParser().Grammar(result.setter));
		return result.result;
	}		
	
	private Terminal parseTerminal(String s) {
		GrammarParsingWithTokens tokens = new GrammarParsingWithTokens();
		
		@SuppressWarnings("rawtypes")
		Parsing<GrammarParsingWithTokens> p = new Parsing<GrammarParsingWithTokens>(tokens, s); 
		ResultObject<Expression> result = new ResultObject<Expression>();
		
		p.parseWithParseTreeConstruction(tokens.new GrammarWithTokensParser().TERMINAL(result.setter));
		return (Terminal) result.result;
	}		
	
	private ArrayList<slope.lib.grammar.parsetree.Token> parseTokenDeclarations(String s) {
		GrammarParsingWithTokens tokens = new GrammarParsingWithTokens();
		
		Parsing<GrammarParsingWithTokens> p = new Parsing<GrammarParsingWithTokens>(tokens, s); 
		
		ResultObject<ArrayList<slope.lib.grammar.parsetree.Token>> result = new ResultObject<ArrayList<slope.lib.grammar.parsetree.Token>>();
		p.parseWithParseTreeConstruction(tokens.new GrammarWithTokensParser().TOKEN_DECLARATIONS(result.setter));
		return result.result;
	}	
	
	private Token parseTokenDeclaration(String s) {
		GrammarParsingWithTokens tokens = new GrammarParsingWithTokens();
		
		@SuppressWarnings("rawtypes")
		Parsing<GrammarParsingWithTokens> p = new Parsing<GrammarParsingWithTokens>(tokens, s); 
		ResultObject<Token> result = new ResultObject<Token>();
		
		p.parseWithParseTreeConstruction(tokens.new GrammarWithTokensParser().TOKEN_DECLARATION(result.setter));
		return result.result;
	}	
}

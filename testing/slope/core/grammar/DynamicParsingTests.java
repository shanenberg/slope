package slope.core.grammar;

import junit.framework.TestCase;
import slope.Parsing;
import slope.ResultObject;
import slope.SetCmd;
import slope.TreeNode;
import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.DynamicGrammarParserFactory;
import slope.lib.grammar.DynamicScanner;
import slope.lib.grammar.GrammarParsing;
import slope.lib.grammar.GrammarParsingWithTokens;
import slope.lib.grammar.GrammarParsing.Grammar01Parser;
import slope.lib.grammar.GrammarParsingWithTokens.GrammarWithTokensParser;
import slope.lib.grammar.parsetree.Expression;
import slope.lib.grammar.parsetree.Grammar;
import slope.treenodecompression.RunSetObjects;

public class DynamicParsingTests extends TestCase {

	 	public void test20_Grammar01() {
	 		String grammar;
	 		grammar = 
	 		"SimpleGrammar"
	 		+ " Start: Grammar"
	 		+ " Rules: "
	 		+ "    Grammar -> <SLOPE IDENTIFIER> \"Start: \" <SLOPE IDENTIFIER> Rules."
	 		+ "    Rules -> \"Rules: \" GrammarRules."
			+ "	   GrammarRules -> GrammarRule+."
			+ "	   GrammarRule -> NonTerminal \"->\" Expression \".\"."
			+ "	   Expression -> Concatenations Alternatives?."
			+ "	   Concatenations -> SimpleExpression+."
			+ "	   Alternatives -> (\"|\" Concatenations)+."
			+ "    SimpleExpression -> (BracketExpression | Terminal | NonTerminal) UnaryOperator?."
			+ "    UnaryOperator -> (\"+\" | \"*\" | \"?\")."
			+ "    BracketExpression -> \"(\" Expression \")\"."
			+ "    NonTerminal -> <SLOPE IDENTIFIER>."
			+ "    Terminal -> LibTerminal | StringTerminal | BuiltInTerminal."
			+ "    LibTerminal -> \"<\" <SLOPE IDENTIFIER> <SLOPE IDENTIFIER>\">\"."		
			+ "    BuiltInTerminal -> \"<\" \"#\" <SLOPE IDENTIFIER> \"#\" \">\"."  
			+ "    StringTerminal -> <# StringLiteral #>."
			+ " TOKENS: "
			+ "   SKIPPED <NewLine> '\n'."
			+ "   SKIPPED <TAB> '\t'."
			+ "   SKIPPED <WhiteSpace> ' '.";
	 		
			parseGrammar(grammar, "G Start: S Rules: S -> \"T\".");

			String grammarWithoutTokens;
	 		grammarWithoutTokens = 
	 		 		"SimpleGrammar"
	 		 		 		+ " Start: Grammar"
	 		 		 		+ " Rules: "
	 		 		 		+ "    Grammar -> <SLOPE IDENTIFIER> \"Start: \" <SLOPE IDENTIFIER> Rules."
	 		 		 		+ "    Rules -> \"Rules: \" GrammarRules."
	 		 				+ "    GrammarRules -> GrammarRule+."
	 		 				+ "	   GrammarRule -> NonTerminal \"->\" Expression \".\"."
	 		 				+ "	   Expression -> Concatenations Alternatives?."
	 		 				+ "	   Concatenations -> SimpleExpression+."
	 		 				+ "	   Alternatives -> (\"|\" Concatenations)+."
	 		 				+ "    SimpleExpression -> (BracketExpression | Terminal | NonTerminal) (\"+\" | \"*\" | \"?\")?."
	 		 				+ "    BracketExpression -> \"(\" Expression \")\"."
	 		 				+ "    NonTerminal -> <SLOPE IDENTIFIER>."
	 		 				+ "    Terminal -> LibTerminal | StringTerminal | BuiltInTerminal."
	 		 				+ "    LibTerminal -> \"<\" <SLOPE IDENTIFIER> <SLOPE IDENTIFIER>\">\"."		
	 		 				+ "    BuiltInTerminal -> \"<\" \"#\" <SLOPE IDENTIFIER> \"#\" \">\"."  
	 		 				+ "    StringTerminal -> <# StringLiteral #>."
					;
			parseGrammar(grammar, grammarWithoutTokens);
	 	}	
	 	
	public void test16_BuiltInTokenStringLiteral() {
			String grammar;
			grammar = 
			"  Simple01 "
			+ "Start:  TestName "
			+ "Rules: "
			+ "  TestName -> <SLOPE IDENTIFIER> \"->\" <SLOPE IDENTIFIER> \".\"."
			+ "  TOKENS: "
			+ "   SKIPPED <WhiteSpace> ' '.";

			parseGrammar(grammar, "a -> a.");
	}		 	
	 	
	 public void test15_BuiltInTokenStringLiteral() {
			String grammar;
			grammar = 
			"  Simple01 "
			+ "Start:  TestName "
			+ "Rules: "
			+ "  TestName -> <SLOPE IDENTIFIER> \"TEST\". "
			+ "  TOKENS: "
			+ "   SKIPPED <WhiteSpace> ' '.";

			parseGrammar(grammar, "abc TEST");
	}		 	
	 	
	 	
	public void test14_BuiltInTokenStringLiteral() {
			String grammar;
			grammar = 
			"  Simple01 "
			+ "Start:  TestName "
			+ "Rules: "
			+ "  TestName -> \"TEST\" <SLOPE IDENTIFIER>."
			+ "  TOKENS: "
			+ "   SKIPPED <WhiteSpace> ' '.";

			parseGrammar(grammar, "TEST abc");
	}		
	 	
	 	
	public void test13_BuiltInTokenStringLiteral() {
		String grammar;
		grammar = 
		"  Simple01 "
		+ "Start:  TestName "
		+ "Rules: "
		+ "  TestName -> <SLOPE IDENTIFIER> \":\" <# StringLiteral #>.";

		parseGrammar(grammar, "abc:\"abcd\"");
	}	
	
	public void test12_BuiltInToken() {
		String grammar;
		grammar = 
		"  Simple01 "
		+ "Start:  Start "
		+ "Rules: "
		+ "  Start -> <# StringLiteral #>.";

		parseGrammar(grammar, "\"abcd\"");
	}		
	
	public void test11_TokenFromLib() {
		String grammar;
		grammar = 
		"  Simple01 "
		+ "Start:  Start "
		+ "Rules: "
		+ "  Start -> <SLOPE IDENTIFIER>.";

		parseGrammar(grammar, "abcd");
	}	
	
	public void test10_N0() {
		String grammar;
		grammar = 
		"  Simple01 "
		+ "Start:  Start "
		+ "Rules: "
		+ "  Start -> <letter>*."
		+ "TOKENS:"
		+ "  <letter> 'a' | 'b'.";

		parseGrammar(grammar, "");
		parseGrammar(grammar, "a");
		parseGrammar(grammar, "aaaaaaabbbababa");
	}
	
	public void test09_NewLineToken() {
		String grammar;
		grammar = 
		"  Simple01 "
		+ "Start:  Start "
		+ "Rules: "
		+ "  Start -> (\"a\" <MyToken>)+."
		+ "TOKENS:"
		+ "  <MyToken> '\n' | '\t'.";

		parseGrammar(grammar, "a\t");
	}		
	
	public void test08_GrammarWithUserDefinedGroup() {
		String grammar;
		grammar = 
		"  Simple01 "
		+ "Start:  Start "
		+ "Rules: "
		+ "  Start ->  <MyToken1> | <MyToken2>."
		+ "TOKENS:"
		+ "  <MyToken1> {'a'-'c'}."
		+ "  <MyToken2> 'z'.";

		parseGrammar(grammar, "a");
		parseGrammar(grammar, "b");
		parseGrammar(grammar, "c");
		parseGrammar(grammar, "z");
	}		
	
	public void test07_GrammarWithUserDefinedGroup() {
		String grammar;
		grammar = 
		"  Simple01 "
		+ "Start:  Start "
		+ "Rules: "
		+ "  Start ->  <MyToken>."
		+ "TOKENS:"
		+ "  <MyToken> {'a'-'c'}.";

		parseGrammar(grammar, "a");
		parseGrammar(grammar, "b");
		parseGrammar(grammar, "c");
	}		
	
	public void test06_GrammarWithUserDefinedToken() {
		String grammar;
		grammar = 
		"  Simple01 "
		+ "Start:  Start "
		+ "Rules: "
		+ "  Start ->  <MyToken>."
		+ "TOKENS:"
		+ "  <MyToken> 'a' | 'b' | 'c' | 'd' | 'e'.";

		parseGrammar(grammar, "a");
		parseGrammar(grammar, "b");
		parseGrammar(grammar, "c");
		parseGrammar(grammar, "d");
		parseGrammar(grammar, "e");
	}			
	
	public void test05_GrammarWithUserDefinedToken() {
		String grammar;
		grammar = 
		"  Simple01 "
		+ "Start:  Start "
		+ "Rules: "
		+ "  Start ->  <MyToken>."
		+ "TOKENS:"
		+ "  <MyToken> 'A'.";

		parseGrammar(grammar, "A");
	}		
	
	public void test04_GrammarWithInnerCall() {
		String grammar;
		grammar = 
		"  Simple01 "
		+ "Start:  Start "
		+ "Rules: "
		+ "  Start ->  A B+ | C."
		+ "  A ->  \"A\"."
		+ "  B ->  \"B\"."
		+ "  C ->  \"C\".";

		parseGrammar(grammar, "ABBB");
		parseGrammar(grammar, "C");
	}		
	
	public void test03_GrammarWithInnerCall() {
		String grammar;
		grammar = 
		"  Simple01 "
		+ "Start:  Start "
		+ "Rules: "
		+ "  Start ->  A B | C."
		+ "  A ->  \"A\"."
		+ "  B ->  \"B\"."
		+ "  C ->  \"C\".";

		parseGrammar(grammar, "AB");
		parseGrammar(grammar, "C");
	}		
	
	public void test03_GrammarWithConcatAndAlternative() {
		String grammar;
		grammar = 
		"  Simple01 "
		+ "Start:  Start "
		+ "Rules: Start ->  \"A\" \"B\" | \"C\".";

		parseGrammar(grammar, "AB");
		parseGrammar(grammar, "C");
		
		grammar = "Simple01 Start:  Start Rules: Start ->  (\"A\" \"B\") | \"C\".";
		parseGrammar(grammar, "AB");
		parseGrammar(grammar, "C");
	
		grammar = "Simple01 Start:  Start Rules: Start ->  (((\"A\") \"B\")) | (\"C\").";
		parseGrammar(grammar, "AB");
		parseGrammar(grammar, "C");

		grammar = "Simple01 Start:  Start Rules: Start ->  ((((((\"A\")) \"B\"))) | (\"C\")).";
		parseGrammar(grammar, "AB");
		parseGrammar(grammar, "C");
	}	
	
	public void test02_GrammarWithAlternative() {
		String grammar;
		grammar = "Simple01 Start:  Start Rules: Start ->  \"A\" | \"B\".";

		parseGrammar(grammar, "A");
		parseGrammar(grammar, "B");

		try {
		  parseGrammar(grammar, "AB");
		  assertTrue("AB is not a valid for for grammar \"A\" | \"B\"", false);
		} catch (Exception ex) {}
		
	}

	public void test02_GrammarWithConcatenation() {
		String grammar;
		grammar = "Simple01 Start:  Start Rules: Start ->  \"Test\" \"ABC\".";
		parseGrammar(grammar, "TestABC");
	}

	
	public void test01_GrammarWithSingleTerminal() {
		String grammar;
		grammar = "Simple01 Start:  Start Rules: Start ->  \"Test\".";
		parseGrammar(grammar, "Test");

		grammar = "Simple01 Start:  Start Rules: Start ->  \"ABC\".";
		parseGrammar(grammar, "ABC");
	
		grammar = "Simple01 Start:  Start Rules: Start ->  \"pqrst\".";
		parseGrammar(grammar, "pqrst");
	}
	
	
	public TreeNode parseGrammar(String grammarString, String word) {
		return parseGrammar(grammarString, word, new ResultObject().setter);
	}	

	public TreeNode parseGrammar(String grammarString, String word, SetCmd result) {
		return this.createGrammar(grammarString).parse(word, result);
	}
	
	public DynamicGrammarParser createGrammar(String grammarString) {
		
		DynamicGrammarParserFactory factory = new DynamicGrammarParserFactory();
		DynamicGrammarParser parser = factory.createParserFromString(grammarString);
		return parser;
		
	}	
	
	
}

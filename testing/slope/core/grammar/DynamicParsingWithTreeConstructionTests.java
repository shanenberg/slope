package slope.core.grammar;

import junit.framework.TestCase;
import slope.Parsing;
import slope.ResultObject;
import slope.SetCmd;
import slope.TreeNode;
import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.DynamicGrammarParserFactory;
import slope.lib.grammar.DynamicScanner;
import slope.lib.grammar.GrammarParsing;
import slope.lib.grammar.GrammarParsingWithTokens;
import slope.lib.grammar.GrammarParsing.Grammar01Parser;
import slope.lib.grammar.GrammarParsingWithTokens.GrammarWithTokensParser;
import slope.lib.grammar.parsetree.Alternative;
import slope.lib.grammar.parsetree.BuiltInTerminal;
import slope.lib.grammar.parsetree.Concatenation;
import slope.lib.grammar.parsetree.Expression;
import slope.lib.grammar.parsetree.Grammar;
import slope.lib.grammar.parsetree.LibraryTerminal;
import slope.lib.grammar.parsetree.MultiplePlus;
import slope.lib.grammar.parsetree.MultipleStar;
import slope.lib.grammar.parsetree.NonTerminal;
import slope.lib.grammar.parsetree.Optional;
import slope.lib.grammar.parsetree.StringTerminal;
import slope.lib.grammar.parsetree.Terminal;
import slope.treenodecompression.RunSetObjects;

public class DynamicParsingWithTreeConstructionTests extends TestCase {

 	public void test16_Optional() {
 		String grammar;
 		grammar = 
 		"  Lambdas "
 		+ "Start: X "
 		+ "	 Rules:"
 		+ "	    X -> <SLOPE IDENTIFIER>?."
 		+ "  TOKENS: "
 		+ "   SKIPPED <Whitespace> ' '."
		;

 		
 		
 		ResultObject result = new ResultObject();
 		parseGrammar(grammar, "x", result.setter);
 		Optional t = (Optional) result.result;
  		LibraryTerminal x = (LibraryTerminal) t.expression;
  		
 		parseGrammar(grammar, "", result.setter);
 		System.out.println(result.result);
 		t = (Optional) result.result;
  		
 	}		
	
 	public void test16_MultiStar() {
 		String grammar;
 		grammar = 
 		"  Lambdas "
 		+ "Start: X "
 		+ "	 Rules:"
 		+ "	    X -> <SLOPE IDENTIFIER>*."
 		+ "  TOKENS: "
 		+ "   SKIPPED <Whitespace> ' '."
		;

 		
 		
 		ResultObject result = new ResultObject();
 		parseGrammar(grammar, "x y", result.setter);

 		MultipleStar t = (MultipleStar) result.result;

 		assertEquals(2, t.values.size());

 		LibraryTerminal x = (LibraryTerminal) t.values.get(0);
 		assertEquals("x", x.value);
 		LibraryTerminal y = (LibraryTerminal) t.values.get(1);
 		assertEquals("y", y.value);
 	}	
	
 	public void test15_LambdaTest() {
 		String grammar;
 		grammar = 
 		"  Lambdas "
 		+ "Start: LambdaTerm "
 		+ "	 Rules:"
 		+ "	    LambdaTerm -> (BracketExpression | Abstraction | Variable)+."
 		+ "	    BracketExpression -> \"(\" LambdaTerm \")\"."
 		+ "	    Abstraction -> \"λ\" Variable \".\" LambdaTerm."
 		+ "	    Variable -> <SLOPE IDENTIFIER>."
 		+ "  TOKENS: "
 		+ "   SKIPPED <NewLine> '\n'."
 		+ "   SKIPPED <Tab> '\t'."
 		+ "   SKIPPED <Whitespace> ' '."
 		;
 		

 		ResultObject resultObject = new ResultObject();
 		DynamicGrammarParser g = this.createGrammar(grammar);
 		g.parse("x");
 		g.parse("x x");
 		g.parse("x x x");

 	}
	
	 	public void test14_TestTest() {
	 		String grammar;
	 		grammar = 
	 		"  GrammarTesting"
	 		+ " Start: Tests"
	 		+ " Rules:"
	 		+ "   Tests -> Test+."
	 		+ "   Test -> Testname \":\" <# StringLiteral #> \".\"."
	 		+ "   Testname -> <SLOPE IDENTIFIER>."
	 		+ " TOKENS:"
	 		+ "   SKIPPED <NewLine> '\n'."
	 		+ "   SKIPPED <Tab> '\t'."
	 		+ "   SKIPPED <Whitespace> ' '.";     

	 		ResultObject resultObject = new ResultObject();
	 		parseGrammar(grammar, 
	 			  "test: \"asd\".\n"
	 			+ "test2: \"adfasfdasdf\".", resultObject.setter);

	 		MultiplePlus p = (MultiplePlus) resultObject.result;

	 		// TODO: Expressions not set yet.
	 		assertEquals(2, p.values.size());
	 		
	 		
	 		for (Expression ex : p.values) {
		 		NonTerminal p2 = (NonTerminal) ex;
		 		Concatenation p3 = (Concatenation) p2.result;
		 		BuiltInTerminal p4 = (BuiltInTerminal) p3.expressions.get(2);
		 		NonTerminal p5 = (NonTerminal) p3.expressions.get(0);
		 		LibraryTerminal p6 = (LibraryTerminal) p5.result;
			}
	 		
	 		
	 	}	     


	
	public void test02_GrammarWithOr() {
		String grammar;
		grammar = "Simple01 Start:  Start Rules: Start ->  \"A\" | \"B\".";
		
		ResultObject r = new ResultObject();

		TreeNode n;
		n = parseGrammar(grammar, "A", r.setter);
		assertTrue (r.result instanceof Alternative);
		assertEquals(1, ((Alternative) r.result).expressions.size());
		assertTrue(((Alternative) r.result).expressions.get(0) instanceof Terminal);
		assertTrue(((Alternative) r.result).expressions.get(0) instanceof StringTerminal);
		StringTerminal t = (StringTerminal) ((Alternative) r.result).expressions.get(0);
		assertEquals("A", t.value);
		
		n = parseGrammar(grammar, "B", r.setter);
		assertTrue (r.result instanceof Alternative);
		assertEquals(1, ((Alternative) r.result).expressions.size());
		assertTrue(((Alternative) r.result).expressions.get(0) instanceof Terminal);
		assertTrue(((Alternative) r.result).expressions.get(0) instanceof StringTerminal);
		t = (StringTerminal) ((Alternative) r.result).expressions.get(0);
		assertEquals("B", t.value);
	}
	
	public void test01_GrammarWithSingleTerminal() {
		String grammar;
		grammar = "Simple01 Start:  Start Rules: Start ->  \"Test\".";
		
		ResultObject r = new ResultObject();
		TreeNode n = parseGrammar(grammar, "Test", r.setter);
		assertTrue (r.result instanceof StringTerminal);
	}
	
	public TreeNode parseGrammar(String grammarString, String word, SetCmd result) {
		
		DynamicGrammarParserFactory factory = new DynamicGrammarParserFactory();
		DynamicGrammarParser parser = factory.createParserFromString(grammarString);
		return parser.parseWithResultObjectCreation(word, result); 
		
	}
	

	public TreeNode parseGrammar(String grammarString, String word) {
		
		DynamicGrammarParserFactory factory = new DynamicGrammarParserFactory();
		DynamicGrammarParser parser = factory.createParserFromString(grammarString);
		return parser.parse(word);
		
	}
	
	public DynamicGrammarParser createGrammar(String grammarString) {
		
		DynamicGrammarParserFactory factory = new DynamicGrammarParserFactory();
		DynamicGrammarParser parser = factory.createParserFromString(grammarString);
		return parser;
		
	}	
	
	
}

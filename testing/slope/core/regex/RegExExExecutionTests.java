package slope.core.regex;

import junit.framework.TestCase;
import shapuc.automata.simple.SimpleAutomata;
import shapuc.automata.simple.SimpleNEA;
import shapuc.util.Pair;
import slope.Parsing;
import slope.ResultObject;
import slope.lib.grammar.parsetree.NonTerminal;
import slope.lib.regex.ext.RegExExtendedParsing;
import slope.lib.regex.simple.*;
import slope.lib.regex.simple.tree.*;;

public class RegExExExecutionTests extends TestCase  {

	public void test04() {
		SimpleNEA<Object, String> nea;
		nea = parseRegEx("{'a'-'z'}+'A'");
		nea.gotoNext("q");
		assertTrue(!nea.hasFinished());
		nea.gotoNext("A");
		assertTrue(nea.hasFinished());
	}

	
	public void test03() {
		SimpleNEA<Object, String> nea;
		nea = parseRegEx("{'a'-'z'}");
		nea.gotoNext("a");
		assertTrue(nea.hasFinished());
	}
	
	public void test02() {
		SimpleNEA<Object, String> nea;
		nea = parseRegEx("'a'('w' | 'x''y''z'?)");
		nea.gotoNext("a");
		assertTrue(!nea.hasFinished());
		nea.gotoNext("w");
		assertTrue(nea.hasFinished());

		nea = parseRegEx("'a'('w' | 'x''y''z'?)");
		nea.gotoNext("a");
		assertTrue(!nea.hasFinished());
		nea.gotoNext("x");
		assertTrue(!nea.hasFinished());
		nea.gotoNext("y");
		assertTrue(nea.hasFinished());
		nea.gotoNext("z");
		assertTrue(nea.hasFinished());
	}	

	
	public void test01() {
		SimpleNEA<Object, String> nea = parseRegEx("'a''a'?");
		nea.gotoNext("a");
		assertTrue(nea.hasFinished());
	}	
	
	public void test00() {
		SimpleNEA<Object, String> nea = parseRegEx("'a'");
		nea.gotoNext("a");
		assertTrue(nea.hasFinished());
	}		
	
	private SimpleNEA parseRegEx(String s) {
		TestConfig<RegEx> testConfig = new TestConfig<RegEx>(s);
		testConfig.parsing.parseWithParseTreeConstruction(
				testConfig.tokens.new RegExExtParser().REGEX(testConfig.result.setter));
		SimpleNEA nea = testConfig.result.result.toNEA();
		nea.start();
		return nea;
	}		
	
	class TestConfig<ResultType> {
		public RegExExtendedParsing tokens;
		public ResultObject<ResultType> result;
		public Parsing<RegExExtendedParsing> parsing;
		
		public TestConfig(String s) {
			 tokens = new RegExExtendedParsing();
			 result = new ResultObject<ResultType>();
			 parsing = new Parsing<RegExExtendedParsing>(tokens, s); 
		}
	}
	
}

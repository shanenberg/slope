package slope.core.regex;

import junit.framework.TestCase;
import shapuc.automata.simple.SimpleAutomata;
import shapuc.automata.simple.SimpleNEA;
import shapuc.util.Pair;
import slope.Parsing;
import slope.ResultObject;
import slope.lib.grammar.parsetree.NonTerminal;
import slope.lib.regex.simple.*;
import slope.lib.regex.simple.tree.*;;

public class RegExExecutionTests extends TestCase  {

	
	public void test09_NewLine() {
		SimpleAutomata<Object, String> nea = parseRegEx("('\\n')*'x'").toNEA();
		nea.start();
		assertTrue(!nea.hasFinished());
		nea.gotoNext("\n");
		assertTrue(!nea.hasFinished());
		nea.gotoNext("\n");
		assertTrue(!nea.hasFinished());
		nea.gotoNext("\n");
		assertTrue(!nea.hasFinished());
		nea.gotoNext("x");
		assertTrue(nea.hasFinished());
	}		


	public void test08_NewLine() {
		SimpleAutomata<Object, String> nea = parseRegEx("('\\n')*").toNEA();
		nea.start();
		assertTrue(nea.hasFinished());
		nea.gotoNext("\n");
		assertTrue(nea.hasFinished());
		nea.gotoNext("\n");
		assertTrue(nea.hasFinished());
		nea.gotoNext("\n");
		assertTrue(nea.hasFinished());
	}		

	public void test07_NewLine() {
		SimpleAutomata<Object, String> nea = parseRegEx("'\\n'").toNEA();
		nea.start();
		nea.gotoNext("\n");
		assertTrue(nea.hasFinished());
	}		
	
	public void test06_Kleene() {
		SimpleNEA<Object, String> nea = parseRegEx("('c')*").toNEA();
		nea.start();
		assertTrue(nea.hasFinished());
		nea.gotoNext("c");
		assertTrue(nea.hasFinished());
		nea.gotoNext("c");
		assertTrue(nea.hasFinished());
		nea.gotoNext("c");
		assertTrue(nea.hasFinished());
	}	
	
	public void test05() {
		SimpleNEA<Object, String> nea = parseRegEx("'a''b'('c')*").toNEA();
		nea.start();
		nea.gotoNext("a");
		assertTrue(!nea.hasFinished());
		nea.gotoNext("b");
		assertTrue(nea.hasFinished());

		nea.gotoNext("c");
		assertTrue(nea.hasFinished());
		nea.gotoNext("c");
		assertTrue(nea.hasFinished());
		nea.gotoNext("c");
		assertTrue(nea.hasFinished());
	}	
	
	public void test04() {
		SimpleNEA<Object, String> nea = parseRegEx("'a''b'|'c'").toNEA();
		nea.start();
		nea.gotoNext("c");
		assertTrue(nea.hasFinished());
	}		

	public void test03() {
		SimpleNEA<Object, String> nea = parseRegEx("'a''b'|'c'").toNEA();
		nea.start();
		nea.gotoNext("a");
		assertTrue(!nea.hasFinished());
		nea.gotoNext("b");
		assertTrue(nea.hasFinished());
	}		

	public void test02() {
		SimpleNEA<Object, String> nea = parseRegEx("'a'|'b'").toNEA();
		nea.start();
		nea.gotoNext("a");
		assertTrue(nea.hasFinished());
	}		
	
	public void test01() {
		SimpleNEA<Object, String> nea = parseRegEx("'a'").toNEA();
		nea.start();
		nea.gotoNext("a");
		assertTrue(nea.hasFinished());
	}	
	
	private RegEx parseRegEx(String s) {
		TestConfig<RegEx> testConfig = new TestConfig<RegEx>(s);
		testConfig.parsing.parseWithParseTreeConstruction(
				testConfig.tokens.new RegExParser().REGEX(testConfig.result.setter));
		return testConfig.result.result;
	}		
	
	class TestConfig<ResultType> {
		public RegExParsing tokens;
		public ResultObject<ResultType> result;
		public Parsing<RegExParsing> parsing;
		
		public TestConfig(String s) {
			 tokens = new RegExParsing();
			 result = new ResultObject<ResultType>();
			 parsing = new Parsing<RegExParsing>(tokens, s); 
		}
	}
	
}

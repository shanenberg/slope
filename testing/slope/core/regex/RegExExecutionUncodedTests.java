package slope.core.regex;

import junit.framework.TestCase;
import shapuc.automata.simple.SimpleAutomata;
import shapuc.automata.simple.SimpleNEA;
import slope.Parsing;
import slope.ResultObject;
import slope.lib.grammar.parsetree.NonTerminal;
import slope.lib.regex.simple.*;
import slope.lib.regex.simple.tree.*;;

public class RegExExecutionUncodedTests extends TestCase  {

	// Uncoded 
	public void test06() {
		SimpleAutomata<Object, String> nea = parseRegEx("'\\n'").toAutomata();
		nea.start();
		nea.gotoNext("\\n");
		assertTrue(nea.hasFinished());
	}		
	
	public void test05() {
		SimpleNEA<Object, String> nea = (SimpleNEA<Object, String>) parseRegEx("'a'('c')*").toAutomata();
		nea.start();
		nea.gotoNext("a");
		assertTrue(nea.hasFinished());
		nea.gotoNext("c");
//System.out.println(nea.toString());		
//System.out.println("#####################");
		assertTrue(nea.hasFinished());
		nea.gotoNext("c");
//System.out.println(nea.toString());		
		assertTrue(nea.hasFinished());
		
	}	
	
	public void test04() {
		SimpleNEA<Object, String> nea = parseRegEx("'a''b'|'c'").toNEA();
		nea.start();
		nea.gotoNext("c");
		assertTrue(nea.hasFinished());
	}		

	public void test03() {
		SimpleNEA<Object, String> nea = parseRegEx("'a''b'|'c'").toNEA();
		nea.start();
		nea.gotoNext("a");
		assertTrue(!nea.hasFinished());
		nea.gotoNext("b");
		assertTrue(nea.hasFinished());
	}		

	public void test02() {
		SimpleNEA<Object, String> nea = parseRegEx("'a'|'b'").toNEA();
		nea.start();
		nea.gotoNext("a");
		assertTrue(nea.hasFinished());
	}		
	
	public void test01() {
		SimpleNEA<Object, String> nea = parseRegEx("'a'").toNEA();
		nea.start();
		nea.gotoNext("a");
		assertTrue(nea.hasFinished());
	}	
	
	private RegEx parseRegEx(String s) {
		TestConfig2<RegEx> testConfig = new TestConfig2<RegEx>(s);
		testConfig.parsing.parseWithParseTreeConstruction(
				testConfig.tokens.new RegExParser().REGEX(testConfig.result.setter));
		return testConfig.result.result;
	}		
	
	class TestConfig2<ResultType> {
		public RegExParsing tokens;
		public ResultObject<ResultType> result;
		public Parsing<RegExParsing> parsing;
		
		public TestConfig2(String s) {
			 tokens = new RegExParsingUncoded();
			 result = new ResultObject<ResultType>();
			 parsing = new Parsing<RegExParsing>(tokens, s); 
		}
	}
	
}

package slope.core.regex;

import junit.framework.TestCase;
import slope.Parsing;
import slope.ResultObject;
import slope.lib.grammar.parsetree.NonTerminal;
import slope.lib.regex.simple.*;
import slope.lib.regex.simple.tree.*;;

public class RegExSimpleParseTreeTests extends TestCase  {

	public void test07SpecialChars() {
		RegEx regex;
		regex = parseRegEx("'\n'");
		assertTrue(regex instanceof Literal);
	}		
	
	public void test06WithKleene() {
		RegEx regex;
		Alternative alternative;
		regex = parseRegEx("'a'*");
		assertTrue(regex instanceof Kleene);
		assertEquals("'a'*", regex.toString());
		Kleene k = (Kleene) regex;
		assertTrue(k.expression instanceof Literal);

		regex = parseRegEx("'b' 'a'*");
		assertTrue(regex instanceof Concatenation);
		Concatenation c = (Concatenation) regex;
		assertEquals(2, c.expressions.size());
		assertTrue(c.expressions.get(1) instanceof Kleene);
	}	
	
	public void test05All() {
		RegEx regex;
		Alternative alternative;
		regex = parseRegEx("'a' 'b' | 'c'");
		assertEquals("'a''b'|'c'", regex.toString());
		assertTrue(regex instanceof Alternative);
		alternative = (Alternative) regex;
		assertTrue(alternative.expressions.get(0) instanceof Concatenation);

		regex = parseRegEx("'a' 'b' | 'c' 'd' 'e' | 'f'");
		assertEquals("'a''b'|'c''d''e'|'f'", regex.toString());
		assertTrue(regex instanceof Alternative);
		alternative = (Alternative) regex;
		assertEquals(3, alternative.expressions.size() ); // Concat becomes Literal if no Concat needed

		regex = parseRegEx("(('a' 'b') 'c' | 'd' 'e') | 'f'");
		assertEquals("(('a''b')'c'|'d''e')|'f'", regex.toString());
		assertTrue(regex instanceof Alternative);
		alternative = (Alternative) regex;
		assertEquals(2, alternative.expressions.size() );
		assertTrue(alternative.expressions.get(0) instanceof BracketExpression);
	
	}
	
	
	public void test04Alternative() {
		RegEx regex;
		Alternative alternative;
		regex = parseRegEx("'a' | 'b'");
		assertEquals("'a'|'b'", regex.toString());
		assertTrue(regex instanceof Alternative); // Concat becomes Literal if no Concat needed
		alternative = (Alternative) regex;
		assertEquals(2, alternative.expressions.size() ); // Concat becomes Literal if no Concat needed

		regex = parseRegEx("'a' | 'b' | 'c'");
		assertEquals("'a'|'b'|'c'", regex.toString());
		assertTrue(regex instanceof Alternative); // Concat becomes Literal if no Concat needed
		alternative = (Alternative) regex;
		assertTrue(alternative.expressions.get(0) instanceof Literal);
		assertTrue(alternative.expressions.get(1) instanceof Literal);
		assertTrue(alternative.expressions.get(2) instanceof Literal);
	}	
	
	public void test03Concat() {
		RegEx regex;
		Concatenation concat;
		regex = parseConcat("'a'");
		assertEquals("'a'", regex.toString());
		assertTrue(regex instanceof Literal); // Concat becomes Literal if no Concat needed

		regex = parseConcat("'a''b'");
		assertEquals("'a''b'", regex.toString());
		concat = (Concatenation) regex;
		assertEquals(2, concat.expressions.size());

		regex = parseConcat("'a''b''c'");
		assertEquals("'a''b''c'", regex.toString());
		concat = (Concatenation) regex;
	}		
	
	public void test02BracketExpression() {
		RegEx regex;
		regex = parseBracketExpression("('a')");
		assertEquals("('a')", regex.toString());
		BracketExpression be = (BracketExpression) regex;
	}		
	
	public void test01NonTerminal() {
		RegEx regex;
		regex = parseLiteral("'a'");
		assertEquals("'a'", ((Literal) regex).character);
	}	

	private RegEx parseRegEx(String s) {
		TestConfig<RegEx> testConfig = new TestConfig<RegEx>(s);
		testConfig.parsing.parseWithParseTreeConstruction(
				testConfig.tokens.new RegExParser().REGEX(testConfig.result.setter));
		return testConfig.result.result;
	}		
	
	
	private RegEx parseConcat(String s) {
		TestConfig<RegEx> testConfig = new TestConfig<RegEx>(s);
		testConfig.parsing.parseWithParseTreeConstruction(
				testConfig.tokens.new RegExParser().REGEX_Concat(testConfig.result.setter));
		return testConfig.result.result;
	}		
	
	private BracketExpression parseBracketExpression(String s) {
		TestConfig<RegEx> testConfig = new TestConfig<RegEx>(s);
		testConfig.parsing.parseWithParseTreeConstruction(
				testConfig.tokens.new RegExParser().REGEX_BracketExpression(testConfig.result.setter));
		return (BracketExpression) testConfig.result.result;
	}		
	
	private Literal parseLiteral(String s) {
		TestConfig<RegEx> testConfig = new TestConfig<RegEx>(s);
		testConfig.parsing.parseWithParseTreeConstruction(
				testConfig.tokens.new RegExParser().REGEX_Literal(testConfig.result.setter));
		return (Literal) testConfig.result.result;
	}	
	
	class TestConfig<ResultType> {
		public RegExParsing tokens;
		public ResultObject<ResultType> result;
		public Parsing<RegExParsing> parsing;
		
		public TestConfig(String s) {
			 tokens = new RegExParsing();
			 result = new ResultObject<ResultType>();
			 parsing = new Parsing<RegExParsing>(tokens, s); 
		}
	}
	
}

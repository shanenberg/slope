/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope.lib.testing;

import junit.framework.TestCase;
import slope.Parsing;
import slope.TreeNode;
import slope.lib.toyexamples.testlang.TestLangParsing;
import slowscane.Scanner;
import slowscane.Tokens;
import slowscane.streams.StringBufferStream;
import slowscane.streams.TokenStream;

public class TestLangParsingTests extends TestCase {
	
    public TokenStream<?> scan(String inputString) {
        Tokens tokens = new TestLangParsing();
        StringBufferStream stringBufferStream = new StringBufferStream(inputString);
        Scanner<?, StringBufferStream> scanner = 
            new Scanner<Tokens, StringBufferStream>(tokens, stringBufferStream);
        return scanner.scan(); 
    }
    
    public void testScan() {
        TokenStream<?> ts = scan("(   \n (     (Int    )     )");
        assertEquals("(", ts.get(0).getValue().toString());
        assertEquals("(", ts.get(1).getValue().toString());
        assertEquals("(", ts.get(2).getValue().toString());
        assertEquals("Int", ts.get(3).getValue().toString());
    }
    
    public TreeNode check(String s) {
    	TestLangParsing tokens = new TestLangParsing();
    	Parsing p = new Parsing(tokens, s); 
    	return p.parse(tokens.new TestLangParser().EXPRESSION());
    	}
    
    public void testParse() {
    	  check("(   (Int) )\n   (abc)   ( ((def)))");
    	} 
	
	
//	
//	public TokenStream<?> scan(String inputString) {
//		Tokens tokens = new TestLangTokens();
//		StringBufferStream stringBufferStream = new StringBufferStream(
//				inputString);
//		Scanner<?, StringBufferStream> scanner = new Scanner<Tokens, StringBufferStream>(
//				tokens, stringBufferStream);
//		return scanner.scan();
//	}
//
//	public void testScan() {
//		TokenStream<?> ts = scan("(   \n (     (Int    )     )");
//		assertEquals("(", ts.get(0).getValue().toString());
//		assertEquals("(", ts.get(1).getValue().toString());
//		assertEquals("(", ts.get(2).getValue().toString());
//		assertEquals("Int", ts.get(3).getValue().toString());
//	}
//
//	public TreeNode check(String s) {
//		TestLangTokens tokens = new TestLangTokens();
//		Parsing p = new Parsing(tokens, s);
//		return p.parse(tokens.new TestLangParser().EXPRESSION());
//	}
//
//	public void testParse() {
//		check("(   (Int) )\n   (abc)   ( ((def)))");
//	}

}
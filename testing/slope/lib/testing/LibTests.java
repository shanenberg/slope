/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope.lib.testing;

import junit.framework.TestCase;
import shapuc.util.Reflection;
import slope.Parsing;
import slope.lib.lambda.v01.Lambda01ParserTokens;
import slope.lib.lambda.v01.Lambda01ParserTokens.Lambda01Parser;
import slowscane.Scanner;
import slowscane.Tokens;
import slowscane.streams.StringBufferStream;

public abstract class LibTests extends TestCase {

	public <TOKENS extends Tokens> 
	Parsing<TOKENS> parse(String string, Class<TOKENS> tokensClass) {
		StringBufferStream stream = new StringBufferStream("x");
		TOKENS tokens = Reflection.instantiateClassUnsave(tokensClass);
		
		Scanner<Tokens, StringBufferStream> scan = 
				new Scanner<Tokens, StringBufferStream>(tokens, stream);
		
		Parsing<TOKENS> parsing = new Parsing<TOKENS>(tokens, scan.scan());
		return parsing;
	}
	

}

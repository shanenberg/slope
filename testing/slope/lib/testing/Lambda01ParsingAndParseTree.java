/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope.lib.testing;

import slope.Parsing;
import slope.ResultObject;
import slope.lib.lambda.v01.Lambda01ParserTokens;
import slope.lib.lambda.v01.parsetree.Abstraction;
import slope.lib.lambda.v01.parsetree.Application;
import slope.lib.lambda.v01.parsetree.BracketExpression;
import slope.lib.lambda.v01.parsetree.LambdaExpression;
import slope.lib.lambda.v01.parsetree.Variable;

public class Lambda01ParsingAndParseTree extends LibTests {


	
	@SuppressWarnings("unchecked")
	public LambdaExpression check(String s) {
		Lambda01ParserTokens tokens = new Lambda01ParserTokens();
		@SuppressWarnings("rawtypes")
		Parsing p = new Parsing(tokens, s); 
		ResultObject<LambdaExpression> result = new ResultObject<LambdaExpression>();
		p.parseWithParseTreeConstruction(tokens.new Lambda01Parser().LAMBDA_EXPRESSION(result.setter));
		return result.result;
	}

	
	public void test01() {
		LambdaExpression le = check("x");
		assertTrue(le instanceof Variable);
		
		assertEquals("x", ((Variable) le).varName);
		
		 le = check("(x)"); 
		assertTrue(le instanceof BracketExpression);		
		assertEquals("x", ((Variable) ((BracketExpression) le).childExpression).varName);

		le = check("(x y)"); 
		assertTrue(le instanceof Application);		
		assertEquals("x", ((Variable) ((Application) le).left).varName);
		assertEquals("y", ((Variable) ((Application) le).right).varName);
			
		le = check("(λa.b)"); 
		assertTrue(le instanceof Abstraction);		
		assertEquals("a", ((Abstraction) le).varName);
		assertEquals("b", ((Variable) ((Abstraction) le).body).varName);
		
	}
	
}

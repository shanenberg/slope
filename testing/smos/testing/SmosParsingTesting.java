/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package smos.testing;

import slope.Parsing;
import slope.ResultObject;
import slope.Rule;
import slope.lib.automata.ext.Automata;
import slope.lib.automata.ext.AutomataParsing;
import slope.lib.automata.ext.Transition;
import slowscane.Tokens;
import junit.framework.TestCase;

public class SmosParsingTesting extends TestCase {
	
	@SuppressWarnings("unchecked")
	public Automata check(String s) {
		AutomataParsing tokens = new AutomataParsing();
		ResultObject<Automata> resultObject = new ResultObject<Automata>();
		@SuppressWarnings("rawtypes")
		Parsing p = new Parsing(tokens, s); 
		
		Rule r = tokens.new AutomataParser().START(resultObject.setter);
		p.parseWithParseTreeConstruction(r);
		return resultObject.result;
	}	
	
	public Transition checkTransition(String s) {
		AutomataParsing tokens = new AutomataParsing();
		Transition t = new Transition();
		@SuppressWarnings("rawtypes")
		Parsing p = new Parsing(tokens, s); 
		p.parseWithParseTreeConstruction(tokens.new AutomataParser().TRANSITION(t));
		return t;
	}	
	
	public Transition checkRule(String s) {
		AutomataParsing tokens = new AutomataParsing();
		ResultObject<Transition> resultObject = new ResultObject<Transition>();
		@SuppressWarnings("rawtypes")
		Parsing p = new Parsing(tokens, s); 
		p.parseWithParseTreeConstruction(tokens.new AutomataParser().RULE(resultObject.setter));
		return resultObject.result;
	}		
	
	public void test_Transition() {
		checkTransition("ε/bbb/initialize");
		checkTransition("ε/-/initialize");
	}
	
	public void test_RULE() {
		checkRule("automataStart --->  ε/-/initialize ---> initialized");
	}	
	
	public void test_Stuff() {
		Automata a1 = check("											" +
				"			QAGameAutomata(S1)											" +
				"																					" +
				"			S1 --->  IN1/-/initialize ---> S2,					" +
				"			S2 --->  IN2/-/- ---> S3,							" +
				"			S3 --->  IN3/-/setNextQuestion ---> S4.				");
		assertEquals(3, a1.transitions.size());

		assertEquals("S1", a1.transitions.get(0).startStateName);
		assertEquals("IN1", a1.transitions.get(0).inMsg);
		assertEquals("S2", a1.transitions.get(0).nextStateName);

		assertEquals("S3", a1.transitions.get(2).startStateName);
		assertEquals("IN3", a1.transitions.get(2).inMsg);
		assertEquals("S4", a1.transitions.get(2).nextStateName);
		
		assertFalse(a1.transitions.get(1)==a1.transitions.get(2));
		
		assertEquals("IN2", a1.transitions.get(1).inMsg);
		assertEquals("S2", a1.transitions.get(1).startStateName);
		assertEquals("S3", a1.transitions.get(1).nextStateName);
	
	
	}
	
	
	public void test_Automata() {
		Automata a1 = check(
		"QAGameAutomata(automataStart)\n"
		  + "  automataStart --->  ε/-/initialize ---> initialized.");
		
		assertEquals("QAGameAutomata", a1.name);
		assertEquals("automataStart", a1.startStateName);
		assertEquals(1, a1.transitions.size());
		assertEquals("automataStart", a1.transitions.get(0).startStateName);
		assertEquals("ε", a1.transitions.get(0).inMsg);
		assertEquals("-", a1.transitions.get(0).conditionMsg);
		assertEquals("initialize", a1.transitions.get(0).outMsg);
		assertEquals("initialized", a1.transitions.get(0).nextStateName);
		
		
		
		a1 = check(
				"QAGameAutomata(automataStart)\n"
				  + "  automataStart --->  ε/-/initialize ---> initialized,\n"
				  + "  initialized --->  ε/-/- ---> createNewQuestion,\n"
				  + "  createNewQuestion --->  ε/-/setNextQuestion ---> askingQuestion.");

		assertEquals(3, a1.transitions.size());
		assertEquals("createNewQuestion", a1.transitions.get(2).startStateName);
		assertEquals("initialized", a1.transitions.get(0).nextStateName);
		
		
	}
}

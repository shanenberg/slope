/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package smos.testing;

import slope.Parsing;
import slope.ResultObject;
import slope.lib.automata.ext.Automata;
import slope.lib.automata.ext.AutomataParsing;
import slope.lib.automata.ext.Transition;
import slowscane.Tokens;
import smos.Context;
import smos.SMOSMachine;
import junit.framework.TestCase;

public class SmosAutomataTesting01 extends TestCase {
	
	public static enum Messages { IN1, IN2, IN3, IN4; }
	public static enum STATES { S1, S2, S3, S4; }
	
	public static class Controller {
		public int checkCounter = 0;
		public boolean checkCondition = true;
		public boolean check(Context c) {
			checkCounter++; 
			return checkCondition;}
		public int initializeCounter = 0;
		public void initialize(Context c) {initializeCounter++;return ;}
		public int setNextQuestionCounter = 0;
		public void setNextQuestion(Context c) {setNextQuestionCounter++;return ;}
	}
	
	final String automataDefinition =
"			QAGameAutomata(S1)											" +
"			S1 --->  ε/check/initialize ---> S2,						" +
"			S2 --->   ε/check/- ---> S3,									" +
"			S3 --->  IN3/-/setNextQuestion ---> S4.						";
			
	SMOSMachine<STATES, Messages> myAutomata = 
			new SMOSMachine<STATES, Messages>(automataDefinition, STATES.class, Messages.class, new Controller());
	
	
	@SuppressWarnings("unchecked")
	public Automata check(String s) {
		AutomataParsing tokens = new AutomataParsing();
		ResultObject<Automata> resultObject = new ResultObject<Automata>();
		@SuppressWarnings("rawtypes")
		Parsing p = new Parsing(tokens, s); 
		p.parseWithParseTreeConstruction(tokens.new AutomataParser().START(resultObject.setter));
		return resultObject.result;
	}	
	
	public void test01() {

		Controller controller = new Controller();
		myAutomata = 
				new SMOSMachine<STATES, Messages>(automataDefinition, STATES.class, Messages.class, controller);		

		// Start-State is the current state after construction
		assertTrue(myAutomata.currentState().equals(STATES.S3));
		assertEquals(2, controller.checkCounter);
		assertEquals(1, controller.initializeCounter);
		
	}
}

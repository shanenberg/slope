/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package sloc.testing;

import java.io.Reader;
import java.io.StringReader;
import java.util.List;

import junit.framework.TestCase;
import sloc.CSVStorageElement;
import sloc.Condition;
import sloc.Header;
import sloc.parsing.CVSHeaderParserTokens;
import sloc.readers.CSVReaderFactory;
import slope.Parsing;
import slope.ResultObject;
import slope.Rule;
import slope.lib.testing.LibTests;

public class CSVHeaderParsing extends LibTests {

	@SuppressWarnings("unchecked")
	public Header check(String s) {
		final ResultObject<Header> resultObject = new ResultObject<Header>();
		CVSHeaderParserTokens tokens = new CVSHeaderParserTokens();
		@SuppressWarnings("rawtypes")
		Parsing p = new Parsing(tokens, s); 
		p.parseWithParseTreeConstruction(tokens.new CVSHeaderParser().START(resultObject.setter));
		return resultObject.result;
	}

	public void test04_Map_String_Int() {
		Header header = check("map:Map(String:Int)"); 
		assertEquals(1, header.columnDefinitions.size());
		assertEquals("map", header.columnDefinitions.get(0).name);
		assertEquals("Map(String:Int)", header.columnDefinitions.get(0).type.getTypeName());
	}
	
	
	public void test03_Int_String_List() {
		Header header = check("id:Int	text:String      aList:List(String)"); 
		assertEquals(3, header.columnDefinitions.size());
		assertEquals("aList", header.columnDefinitions.get(2).name);
		assertEquals("List(String)", header.columnDefinitions.get(2).type.getTypeName());
	}
	
	
	public void test02_Int_String() {
		Header header = check("id:Int	text:String      "); 
		assertEquals(2, header.columnDefinitions.size());
		assertEquals("id", header.columnDefinitions.get(0).name);
		assertEquals("Int", header.columnDefinitions.get(0).type.getTypeName());
		assertEquals("text", header.columnDefinitions.get(1).name);
		assertEquals("String", header.columnDefinitions.get(1).type.getTypeName());
	}	
	public void test_Int() {
		Header header = check("id:Int"); 
		assertEquals(1, header.columnDefinitions.size());
		assertEquals("id", header.columnDefinitions.get(0).name);
	}


}

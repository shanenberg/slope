/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

package sloc.testing;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import sloc.Header;
import sloc.parsing.CVSHeaderParserTokens;
import sloc.parsing.CVSHeaderParserTokens.CVSHeaderParser;
import slope.Parsing;
import slope.ResultObject;
import slope.lib.testing.LibTests;

public class CSVBodyParsing extends LibTests {

	@SuppressWarnings("unchecked")
	public void check(String headerString, String bodyString,
			Object targetObject) {

		ResultObject<Header> resultObject = new ResultObject<Header>();

		CVSHeaderParserTokens tokens = new CVSHeaderParserTokens();
		@SuppressWarnings("rawtypes")
		Parsing p = new Parsing(tokens, headerString);
		p.parseWithParseTreeConstruction(tokens.new CVSHeaderParser()
				.START(resultObject.setter));
		resultObject.result.parseLineInto(bodyString, targetObject);
		;
	}
	
	public void test_Maps() {

		class Result {
			public Map myMap = new HashMap();
		};

		Result result = new Result();
		check("myMap:Map(String:Int)", "{}", result);
		assertEquals(0, result.myMap.size());
		
		result = new Result();
		check("myMap:Map(String:Int)", "{(\"test\":1)}", result);
		assertEquals(1, result.myMap.size());
		assertTrue(result.myMap.containsKey("test"));
		assertEquals(1, result.myMap.get("test"));

		result = new Result();
		check("myMap:Map(String:Int)", "{(\"test1\":1), (\"test2\":2)}", result);
		assertTrue(result.myMap.containsKey("test1"));
		assertEquals(1, result.myMap.get("test1"));
		assertEquals(2, result.myMap.size());

		result = new Result();
		check("myMap:Map(String:Int)", "{(\"test\":NULL), (NULL:2)}", result);

		class IntResult {
			public int i;
		};
		
		IntResult intResult = new IntResult();
		check("i:Hexa", "0x3d22c6", intResult);
		assertEquals(0x3d22c6, intResult.i);

	}
	
	public void test01() {
		class Test01 {
			public Integer id;
		}
		;
		Test01 t01 = new Test01();
		check("id:Int", "NULL", t01);
		assertEquals(null, t01.id);

		t01 = new Test01();
		check("id:Int", "42", t01);
		assertEquals(42, t01.id.intValue());

		class Test02 {
			public String aString;
		}
		;
		Test02 t02 = new Test02();
		check("aString:String", "\"Stefan\"", t02);
		assertEquals("Stefan", t02.aString);

		class Test03 {
			public List<Integer> aList;
		}
		;
		Test03 t03 = new Test03();
		check("aList:List(Int)", "{42}", t03);
		assertEquals(1, t03.aList.size());
		assertEquals(42, t03.aList.get(0).intValue());

		t03 = new Test03();
		check("aList:List(Int)", "{42, 43}", t03);
		assertEquals(2, t03.aList.size());
		assertEquals(42, t03.aList.get(0).intValue());
		assertEquals(43, t03.aList.get(1).intValue());

		class Test04 {
			public List<List<Integer>> aList;
		}
		;

	}
	
	public void test02() {
//		Test04 t04 = new Test04();
//		check("mapid:Int name:String required:List(Int)", t04);
//		assertEquals(1, t04.aList.size());
//		assertEquals(1, t04.aList.get(0).size());
//		assertEquals(42, t04.aList.get(0).get(0).intValue());
//
//		
//		mapid:Int	name:String				required:List(Int)
//		1			"Kontinente"			()
//		
	}

}
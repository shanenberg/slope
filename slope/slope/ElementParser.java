/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope;

import slowscane.Tokens;
import slowscane.streams.TokenStream;

public abstract class ElementParser<TREENODETYPE extends TreeNode> extends Parser<Tokens> { 
	
	// The name of the treeNode constructed by this parser
//	public final String treeNodeName;
	
//	private final String pcTypeName;

	public ElementParser(String treeNodeName) {
		super();
//		this.pcTypeName = treeNodeName;
	}

	public TREENODETYPE saveParse(TokenStream<?> tokenstream) {
		int setBackPoint = tokenstream.currentPosition;
		try {
			return parse(tokenstream);
		} catch (RuntimeException ex) {
			tokenstream.currentPosition = setBackPoint;
			throw ex;
		}
	}	

	public abstract TREENODETYPE parse(TokenStream<?> tokenstream);
	
	public ParseException createParseException(TokenStream<?> stream, String additionalString) {
		String tokenStreamMSG = stream.stuckExceptionStringUpTo(
				stream.currentPosition>0?stream.currentPosition - 1:0);
		return new ParseException(additionalString, tokenStreamMSG);
	}

	public void addTreeNodeToTreeNodeN(TreeNode n, TreeNodeImpl_N ret) {
		ret.add(n);
	}
}

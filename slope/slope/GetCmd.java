package slope;

public interface GetCmd<TYPE> {
	public TYPE get();
}

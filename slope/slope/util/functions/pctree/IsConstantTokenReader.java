package slope.util.functions.pctree;

import shapuc.refvis.SingleParamDispatcher;
import slope.PC;
import slope.lang.TOKEN.TokenParserConstructor;
import slowscane.scanners.KeywordScanner;
import slowscane.scanners.SingleCharScanner;
import slowscane.scanners.StringElementScanner;
import slowscane.scanners.TokenReader;

public class IsConstantTokenReader extends SingleParamDispatcher<Boolean, TokenReader> {

	public IsConstantTokenReader() {
		super("isTokenReader");
	}

	public Boolean isTokenReader(TokenReader pc) {
		return false;
	}

	public Boolean isTokenReader(KeywordScanner pc) {
		return true;
	}

	public Boolean isTokenReader(SingleCharScanner pc) {
		return true;
	}
}

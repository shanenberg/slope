package slope.util.functions.pctree;

import shapuc.refvis.SingleParamDispatcher;
import slope.PC;
import slope.lang.TOKEN.TokenParserConstructor;

/**
 * Everyone except the TokenParserConstructor is a is NOT a TokenParserConstructor. 
 *
 */
public class IsTokenParserConstructor extends SingleParamDispatcher<Boolean, PC> {
	
	public IsTokenParserConstructor() {
		super("isTokenParserConstructor");
	}
	
	public Boolean isTokenParserConstructor(PC pc) {
		return false;
	}
	
	public Boolean isTokenParserConstructor(TokenParserConstructor pc) {
		return true;
	}	
}

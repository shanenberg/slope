/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope;

import slope.lib.automata.ext.AutomataParsing;
import slope.treenodecompression.RunSetObjects;
import slowscane.Scanner;
import slowscane.Tokens;
import slowscane.streams.StringBufferStream;
import slowscane.streams.TokenStream;

/**
 * Parsing is done by passing a TOKEN object and a tokenStream to the parser.
 * Typically, this is done by the following code:
 * 
 * 		TOKENS tokens = new ...myTokensClass..();
		StringBufferStream stream = new StringBufferStream(automataString);
		Scanner<Tokens, StringBufferStream> scan = 
				new Scanner<Tokens, StringBufferStream>(tokens, stream);
		
		Parsing parsing = new Parsing(tokens, scan.scan());

 * @author Stefan Hanenberg (stefan.hanenberg@gmail.com)
 *
 * @param <TOKENS>
 */
public class Parsing<TOKENS extends Tokens> {
	
	public final TokenStream<?> tokenStream;
	public final TOKENS tokens;
	
	/**
	 * 
	 * @param tokens
	 * @param tokenStream
	 */
	public Parsing(TOKENS tokens, TokenStream<?> tokenStream) {
		super();
		this.tokenStream = tokenStream;
		this.tokens = tokens;
	}

	public TOKENS getTokens() {
		return tokens;
	}

	public Parsing(TOKENS tokens, String string) {
		this.tokens = tokens;
		Scanner<?, StringBufferStream> scanner = 
				new Scanner<Tokens, StringBufferStream>(tokens, new StringBufferStream(string));
		tokenStream = scanner.scan();
	}
	
	public TreeNode parse(PC<?> parserConstructor) {
		ElementParser<? extends TreeNode> p = parserConstructor.createParser();
		TreeNode treeNode = p.parse(tokenStream); 

		if (!tokenStream.isEOF())
			throw p.createParseException(tokenStream, "Haven't reached EOF.");
			
		return treeNode;
	}
	
	public TreeNode parseWithParseTreeConstruction(PC<?> parserConstructor) {
		TreeNode n = this.parse(parserConstructor);
		RunSetObjects bindObject = new RunSetObjects();
		n.accept(bindObject);
		return n;
	}
}

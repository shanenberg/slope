/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope.lib.toyexamples;

import slope.PC;
import slope.Parser;
import slope.Rule;
import slowscane.Tokens;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.scanners.TokenReader;

public class DotsNBracketsParsing extends Tokens {
		
		// Whitespaces are separators but not used while parsing (i.e. they are skipped)
		@Separator public TokenReader<?> WHITESPACE() {	return lib.WHITESPACE; 	}
		
		@Token public TokenReader<?> DOT() { return lib.DOT;}
		@Token public TokenReader<?> OPEN_BRACKET() { return lib.OPEN_BRACKET; }
		@Token public TokenReader<?> CLOSED_BRACKET() { return lib.CLOSED_BRACKET; }
		
		public class DotsNBracketsParser extends Parser<DotsNBracketsParsing> {

			public Rule START() { return new Rule() { public PC pc() { return
				OR(
					BRACKET_EXPRESSION(), 
					TOKEN(DOT()));
			}};}
			
			public Rule BRACKET_EXPRESSION() { return new Rule() { public PC pc() { return
				AND( TOKEN('('),START(), TOKEN(')'));
			}};}			
		}

}

/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope.lib.toyexamples;

import slope.PC;
import slope.Parser;
import slope.Rule;
import slowscane.Tokens;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.scanners.TokenReader;

public class ExpressionsNNumbersParsing extends Tokens {
		
		// Whitespaces are separators but not used while parsing (i.e. they are skipped)
		@Separator public TokenReader<?> WHITESPACE() {	return lib.WHITESPACE; 	}
		
		@Token public TokenReader<?> NUMBER() { return lib.JAVA_INTEGER_LITERAL; }

		@Token public TokenReader<?> PLUS() { return lib.PLUS; }
		@Token public TokenReader<?> MINUS() { return lib.MINUS; }
		@Token public TokenReader<?> MULT() { return lib.MULT; }
		@Token public TokenReader<?> DIV() { return lib.DIV; }
		
		@Token public TokenReader<?> OPEN_BRACKET() { return lib.OPEN_BRACKET; }
		@Token public TokenReader<?> CLOSED_BRACKET() { return lib.CLOSED_BRACKET; }
		
		public class ExpressionsNNumbersParser extends Parser<ExpressionsNNumbersParsing> {

			public Rule START() { return new Rule() { public PC pc() { return
				EXPRESSION();
			}};}
			
			public Rule EXPRESSION() { return new Rule() { public PC pc() { return 
					OR(COMPLEX_EXPRESSION(),SIMPLE_EXPRESSION(), BRACKET_EXPRESSION());
			}};}
				
			public Rule SIMPLE_EXPRESSION() { return new Rule() { public PC pc() { return 
				TOKEN(NUMBER());
			}};}
				
			public Rule COMPLEX_EXPRESSION() { return new Rule() { public PC pc() { return 
				AND(
					OR(
						SIMPLE_EXPRESSION(), BRACKET_EXPRESSION()),
					AND(
						OPERATION(),
						EXPRESSION()));
			}};}			
				
			public Rule OPERATION() { return new Rule() { public PC pc() { return 
				OR(TOKEN('+'), TOKEN('-'), TOKEN('*'), TOKEN('/'));
			}};}	
			
			public Rule BRACKET_EXPRESSION() { return new Rule() { public PC pc() { return
				AND(
					TOKEN('('), 
					OR(BRACKET_EXPRESSION(), COMPLEX_EXPRESSION()), 
					TOKEN(')'));
			}};}			
		
		}
		
}

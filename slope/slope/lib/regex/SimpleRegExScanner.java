package slope.lib.regex;

import shapuc.automata.simple.SimpleAutomata;
import slope.Parsing;
import slope.ResultObject;
import slope.lib.regex.simple.RegExParsing;
import slope.lib.regex.simple.RegExParsing.RegExParser;
import slope.lib.regex.simple.tree.RegEx;
import slowscane.Token;
import slowscane.scanners.TokenReader;
import slowscane.scanners.lib.JavaCharLiteralScanner;
import slowscane.streams.PositionStream;

public class SimpleRegExScanner extends TokenReader<String>{

	String regex;
	SimpleAutomata<Object, String> automata;
	
	public SimpleRegExScanner(String regex) {
		this.regex = regex;
		this.automata = createAutomata();
	}
	
	private SimpleAutomata<Object, String> createAutomata() {
		RegExParsing tokens = new RegExParsing();
		ResultObject<RegEx> result = new ResultObject<RegEx>();
		Parsing<RegExParsing> parsing = new Parsing<RegExParsing>(tokens, regex);

		parsing.parseWithParseTreeConstruction(tokens.new RegExParser().REGEX(result.setter));
		return result.result.toAutomata();
	}

	@Override
	public Token<String> scanNext(PositionStream<?> stream) {
		throw new RuntimeException("Stefan: everything sucks here");
//		
//		JavaCharLiteralScanner charLiteralScanner = new JavaCharLiteralScanner();
//		
//		int startPosition = stream.currentPosition;
//		String lastFinishedWord = null;
//		int lastCorrectPosition = startPosition;
//		
//		StringBuffer currentWord = new StringBuffer();
//		
//		while(true) {
//			Token<String> next = charLiteralScanner.scanNext(stream);
//			currentWord.append(next.getValue());
//			if (automata.hasFinished()) {
//			}
//		}
//		
//		return null;
	}

}

package slope.lib.regex.simple;

import slope.Cmd;
import slope.GetCmd;
import slope.PC;
import slope.Parser;
import slope.ResultObject;
import slope.Rule;
import slope.SetCmd;
import slope.lib.testing.SimpleExpression;
import slowscane.Tokens;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.annotations.TokenLevel;
import slowscane.scanners.TokenReader;
import slope.lib.grammar.parsetree.Expression;
import slope.lib.regex.simple.tree.*;

/*
   The RegExParsingUncoded translates a RegEx in an uncoded way.
   For example, the token "\n" is not translated into a newLine, but into the
   Token "\\n"
 */

public class RegExParsingUncoded extends RegExParsing {
	@TokenLevel(50) public TokenReader<String> JAVA_CHARACTER_LITERAL() { return lib.JAVA_CHARACTER_LITERAL_UNCODED; }
}



package slope.lib.regex.simple;

import slope.Cmd;
import slope.GetCmd;
import slope.PC;
import slope.Parser;
import slope.ResultObject;
import slope.Rule;
import slope.SetCmd;
import slope.lib.testing.SimpleExpression;
import slowscane.Tokens;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.annotations.TokenLevel;
import slowscane.scanners.TokenReader;
import slope.lib.grammar.parsetree.Expression;
import slope.lib.regex.simple.tree.*;

/*
   RegEx:
   
     RegEx ->  CancatSimplEx ('|' CancatSimplEx)*.
     CancatSimplEx -> SimpleRegEx+.
     SimpleRegEx -> (BracketExpression | Literal) '*'?.
     BracketExpression -> '(' RegEx ')'.
     Literal -> <JavaCharacterLiteralScanner>.
   
 */

public class RegExParsing extends Tokens {
	
	@Separator public TokenReader<?> WHITESPACE() { return lib.WHITESPACE; }

	@TokenLevel(50) public TokenReader<String> JAVA_CHARACTER_LITERAL() { return lib.JAVA_CHARACTER_LITERAL; }
	@TokenLevel(50) public TokenReader<?> KLEENE() { return lib.MULT; }
	@TokenLevel(50) public TokenReader<?> ALTERNATIVE() { return lib.OR;}
	@TokenLevel(50) public TokenReader<?> OBRACKET() { return lib.OPEN_BRACKET; }
	@TokenLevel(50) public TokenReader<?> CBRACKET() { return lib.CLOSED_BRACKET; }

	public class RegExParser extends Parser<RegExParsing> {
		
		public Rule REGEX(final SetCmd<RegEx> resultSetter) { return new Rule() { public PC pc() {
			final ResultObject<RegEx> regex = new ResultObject<RegEx>();
			
			final SetCmd<RegEx> resetHierarchySetter = new SetCmd<RegEx>() {
				public void set(RegEx alternative) {
					RegEx r = regex.getResult();
					if (!(r instanceof Alternative)) {
						regex.result = alternative;
						((Alternative) alternative).expressions.add(0, r);
					} else {
						((Alternative) r).expressions.addAll(((Alternative) alternative).expressions);
					}
				}
			};
			
			return
				AND(
					WITH_OPTIONAL(
						REGEX_Concat(regex.setter),
						REGEX_Alternative(resetHierarchySetter)
					),
					result(resultSetter, regex.getter)
				);
		}};}
		
		public Rule REGEX_Alternative(final SetCmd<RegEx> resultSetter) { return new Rule() { public PC pc() {
			final ResultObject<Alternative> result = ResultObject.create(new Alternative());
			SetCmd<RegEx> alternativeAdder = addResultSetter(result.result.expressions);
			
			return
				AND(
					NFOLD(
							AND(
								TOKEN('|'),	
								NFOLD(REGEX_Concat(alternativeAdder))
							)
					),
					result(resultSetter, (GetCmd)  result.getter)
				);
		}};}		

		public Rule REGEX_Concat(final SetCmd<RegEx> resultSetter) { return new Rule() { public PC pc() {
			final Concatenation concat = new Concatenation();
			SetCmd<RegEx> concatAdder = addResultSetter(concat.expressions);
			Cmd replaceConcatIfNecessary = new Cmd() {
				public void doCmd() {
					if (concat.expressions.size()==1) {
						resultSetter.set(concat.expressions.get(0));
					} else {
						resultSetter.set(concat);
					}
				}
			};
			
			return 
				AND(
					NFOLD(REGEX_SimpleExpression(concatAdder)),
					set(replaceConcatIfNecessary)
				);
		}};}		
	
		public Rule REGEX_SimpleExpression(final SetCmd<RegEx> resultSetter) { return new Rule() { public PC pc() { 

			final ResultObject<RegEx> result = new ResultObject<RegEx>();
			SetCmd<UnaryRegEx> kleenePushup = new SetCmd<UnaryRegEx>() {
				public void set(UnaryRegEx value) {
					RegEx r = result.getResult();
					result.result = value;
					value.expression = r;
				}
			};
			
			return 
				AND(
					WITH_OPTIONAL(
						OR(
							REGEX_BracketExpression(result.setter), 
							REGEX_Literal(result.setter)),
						REGEX_POST_UNARYOPERATOR(kleenePushup)
					),
					result(resultSetter, result.getter)
				);			
		}};}	
		
		public Rule REGEX_POST_UNARYOPERATOR(final SetCmd<UnaryRegEx> result) { return new Rule() { public PC pc() { 
			return 
				AND( TOKEN(KLEENE()), result(result, new Kleene()) );			
		}};}	
		
		
		
		
		public Rule REGEX_BracketExpression(final SetCmd<RegEx> resultSetter) { return new Rule() { public PC pc() {
			final BracketExpression bracketExpression = new BracketExpression();
			SetCmd<RegEx> set = new SetCmd<RegEx>() {
				public void set(RegEx value) {bracketExpression.expression = value;}};
			
			return 
				AND(
					TOKEN('('), REGEX(set), TOKEN(')'),
					result(resultSetter, bracketExpression));			
		}};}
		
		public Rule REGEX_Literal(final SetCmd<RegEx> resultSetter) { return new Rule() { public PC pc() {
			final Literal literal = new Literal();
			SetCmd<String> set = new SetCmd<String>() {
				public void set(String value) {	literal.character = value;} };
				
			return 
				AND(
					TOKEN(JAVA_CHARACTER_LITERAL(), set),
					result(resultSetter, literal));			
		}};}		
		
	}
	
}



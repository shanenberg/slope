package slope.lib.regex.simple.tree;

import shapuc.automata.simple.SimpleNEA;

public class BracketExpression extends UnaryRegEx {

	@Override
	public void printTo(StringBuffer buffer) {
		buffer.append("(");
		expression.printTo(buffer);
		buffer.append(")");
	}

	@Override
	public void toNEA(SimpleNEA<Object, String> automata, Object startNode, Object endNode) {
		expression.toNEA(automata, startNode, endNode);
		
	}

}

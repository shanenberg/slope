package slope.lib.regex.simple.tree;

import shapuc.automata.simple.SimpleAutomata;
import shapuc.automata.simple.SimpleNEA;

/**
 * Root class for regular expressions.
 * 
 * @author stefan
 *
 */
public abstract class RegEx {
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		printTo(buffer);
		return buffer.toString();
	}
	
	public abstract void printTo(StringBuffer buffer);
	
	public abstract void toNEA(SimpleNEA<Object, String> automata, Object startNode, Object endNode);

	public SimpleNEA<Object, String> toNEA() {
		
		SimpleNEA<Object, String> automata = new SimpleNEA<Object, String>();
		Object start = new Object(); Object end = new Object();
		
		automata.setSpontaneousInput(new String("ε"));
		
		automata.setStartState(start);
		automata.addEndState(end);

		this.toNEA(automata, start, end);
		
		return automata;
	}
	
	public SimpleAutomata<Object, String> toAutomata() {
		return toNEA();
	}
}

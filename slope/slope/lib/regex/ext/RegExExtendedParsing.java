package slope.lib.regex.ext;

import slope.PC;
import slope.Parser;
import slope.ResultObject;
import slope.Rule;
import slope.SetCmd;
import slope.lib.lambda.v01.parsetree.LambdaExpression;
import slope.lib.regex.ext.tree.KleenePlus;
import slope.lib.regex.ext.tree.Optional;
import slope.lib.regex.ext.tree.LiteralGroup;
import slope.lib.regex.simple.RegExParsing;
import slope.lib.regex.simple.tree.Kleene;
import slope.lib.regex.simple.tree.Literal;
import slope.lib.regex.simple.tree.RegEx;
import slope.lib.regex.simple.tree.UnaryRegEx;
import slowscane.Tokens;
import slowscane.annotations.Separator;
import slowscane.annotations.TokenLevel;
import slowscane.scanners.TokenReader;


/*
RegEx:

  RegEx -> SimpleRegEx* ('|' SimpleRegEx* )*.
  SimpleRegEx -> (BracketExpression | Literal | LiteralGroup) ('?' | '+' | '*')?
  BracketExpression -> '(' RegEx ')'.
  Literal -> <JavaCharacterLiteral>.
  LiteralGroup -> '{' LiteralIntervalGroup '}'  
  LiteralIntervalGroup -> JavaCharacterLiteral '-' JavaCharacterLiteral.

*/


public class RegExExtendedParsing extends RegExParsing {
	
	@Separator public TokenReader<?> WHITESPACE() { return lib.WHITESPACE; }

	@TokenLevel(50) public TokenReader<?> QUESTION_MARK() { return lib.QUESTION_MARK; }
	@TokenLevel(50) public TokenReader<?> PLUS() { return lib.PLUS; }
	@TokenLevel(50) public TokenReader<?> MINUS() { return lib.MINUS; }
	@TokenLevel(50) public TokenReader<?> OBRACKET() { return lib.OPEN_BRACKET; }
	@TokenLevel(50) public TokenReader<?> CBRACKET() { return lib.CLOSED_BRACKET; }
	@TokenLevel(50) public TokenReader<?> OCURLY_BRACKET() { return lib.OPEN_CURELY_BRACKET;}
	@TokenLevel(50) public TokenReader<?> CCURLY_BRACKET() { return lib.CLOSED_CURLY_BRACKET;}

	public class RegExExtParser extends RegExParser {
		
		/** NEW: QuestionMark + Plus */
		public Rule REGEX_POST_UNARYOPERATOR(final SetCmd<UnaryRegEx> result) { return new Rule() { public PC pc() { 
			return 
				OR(
					AND( TOKEN(KLEENE()), result(result, new Kleene())),	
					AND( TOKEN(PLUS()), result(result, new KleenePlus())),	
					AND( TOKEN(QUESTION_MARK()), result(result, new slope.lib.regex.ext.tree.Optional()))
				);
		}};}
	
	public Rule REGEX_Literal(final SetCmd<RegEx> resultSetter) { return new Rule() { public PC pc() {
		final ResultObject<RegEx> result = new ResultObject<RegEx>();
			
		return 
			AND(
				OR(
					REGEX_SimpleLiteral(result.setter), REGEX_LiteralGroup(result.setter)
				),
				result(resultSetter, result.getter));			
	}};}	
	
	public Rule REGEX_SimpleLiteral(final SetCmd<RegEx> resultSetter) { return new Rule() { public PC pc() {
		final Literal literal = new Literal();
		SetCmd<String> set = new SetCmd<String>() {
			public void set(String value) {	literal.character = value;} };
			
		return 
			AND(
				TOKEN(JAVA_CHARACTER_LITERAL(), set),
				result(resultSetter, literal));			
	}};}	
	
	public Rule REGEX_LiteralGroup(final SetCmd<RegEx> resultSetter) { return new Rule() { public PC pc() {
		final LiteralGroup literal = new LiteralGroup();
		SetCmd<RegEx> setLeft = new SetCmd<RegEx>() {
			public void set(RegEx value) {
				literal.start = (slope.lib.regex.simple.tree.Literal) value;
			}
		};
		
		SetCmd<RegEx> setRight = new SetCmd<RegEx>() {
			public void set(RegEx value) {
				literal.end = (slope.lib.regex.simple.tree.Literal) value;
			}
		};		
			
		return 
			AND(
				TOKEN(OCURLY_BRACKET()),
				REGEX_SimpleLiteral(setLeft),
				TOKEN(MINUS()),
				REGEX_SimpleLiteral(setRight),
				TOKEN(CCURLY_BRACKET()),
				result(resultSetter, literal));			
	}};}	
	
	}
	
	
}

package slope.lib.regex.ext.tree;

import java.util.List;

import shapuc.automata.simple.SimpleNEA;
import slope.lib.regex.simple.tree.Alternative;
import slope.lib.regex.simple.tree.Literal;
import slope.lib.regex.simple.tree.RegEx;

public class LiteralGroup extends RegEx {

	public Literal start;
	public Literal end;

	
	public void printTo(StringBuffer buffer) {
		buffer.append("{" + start + "-" + end +"}"); 
	}

	@Override
	public void toNEA(SimpleNEA<Object, String> automata, Object startNode, Object endNode) {
		List<Literal> allLiterals = start.literalsUpTo(end);

		Alternative alternative = new Alternative();

		alternative.expressions.addAll(allLiterals);
		
		alternative.toNEA(automata, startNode, endNode);
	}

	
}
package slope.lib.regex.ext.tree;

import shapuc.automata.simple.SimpleNEA;
import slope.lib.regex.simple.tree.UnaryRegEx;

public class Optional extends UnaryRegEx {

	@Override
	public void printTo(StringBuffer buffer) {
		expression.printTo(buffer);
		buffer.append("?");
	}

	@Override
	public void toNEA(SimpleNEA<Object, String> automata, Object startNode, Object endNode) {
		Object myStart = new Object();
		Object myEnd = new Object();
		automata.addSpontaneousTransition(startNode, myStart);
		automata.addSpontaneousTransition(myEnd, endNode);
		automata.addSpontaneousTransition(myStart, myEnd); // From start to end -> 0 times
		
		expression.toNEA(automata, myStart, myEnd);
	}

}

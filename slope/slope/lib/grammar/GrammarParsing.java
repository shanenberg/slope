package slope.lib.grammar;

import slope.Cmd;
import slope.GetCmd;
import slope.PC;
import slope.Parser;
import slope.ResultObject;
import slope.Rule;
import slope.SetCmd;
import slope.lib.grammar.parsetree.*;
import slowscane.Tokens;
import slowscane.annotations.Keyword;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.annotations.TokenLevel;
import slowscane.lib.Lambda01.Lambda01Tokens;
import slowscane.scanners.KeywordScanner;
import slowscane.scanners.TokenReader;
import slowscane.scanners.lib.JavaStringLiteralScanner;

/*
   SimpleGrammar
   Start: GrammarRules
   Rules:
	GrammarRules -> Rule+.
	GrammarRule -> NonTerminal "->" Expression. 
	ObjektReturn -> TypeName (propertyName ":" localVariable)+.
	LAReturn -> "LA" "(" identifier "," identifier "(" identifier identifier ")" ")".
	Expression -> SimpleExpression+ (ReturnCode)? (("|" (SimpleExpression)+)*. 
    SimpleExpression -> (BracketExpression | Terminal | NonTerminal) 
    BracketExpression -> "(" Expression ")".
 */

public class GrammarParsing extends Tokens {
	@Separator public TokenReader<?> JAVA_BLOCK_COMMENT() { return lib.JAVA_BLOCK_COMMENT; }
	@Separator public TokenReader<?> WHITESPACE() {	return lib.WHITESPACE; 	}
	@Separator public TokenReader<?> TAB() {	return lib.TAB; }
	@Separator public TokenReader<?> NEWLINE() {	return lib.NEWLINE; }
	
	@Token public TokenReader<?> TOKEN_OR() { return lib.OR; }
	@Token public TokenReader<?> DOT() { return lib.DOT; }
	@TokenLevel(100) public TokenReader<?> COLON() { return lib.COLON; }
	@Token public TokenReader<?> COMMA() { return lib.COMMA; }
	@Token public TokenReader<?> PLUS() { return lib.PLUS; }
	@Token public TokenReader<?> EQUALS() { return lib.EQUALS; }
	@Token public TokenReader<?> CARET() { return lib.CARET; }
	@Token public TokenReader<?> OBRACKET() { return lib.OPEN_BRACKET; }
	@Token public TokenReader<?> CBRACKET() { return lib.CLOSED_BRACKET; }
	@Token public TokenReader<?> OCURLY_BRACKET() { return lib.OPEN_CURELY_BRACKET; }
	@Token public TokenReader<?> CCURLY_BRACKET() { return lib.CLOSED_CURLY_BRACKET; }
	@Token public TokenReader<?> QUESTION_MARK() { return lib.QUESTION_MARK; }
	
	@TokenLevel(5) public TokenReader<?> BUILDINTERMINAL_START() { return new KeywordScanner("<#"); }
	@TokenLevel(5) public TokenReader<?> BUILDINTERMINAL_END() { return new KeywordScanner("#>"); }
	
	@TokenLevel(5) public TokenReader<?> SHORT_ARROW() { return lib.KEYWORD_SHORT_ARROW; }
	
	@Token public TokenReader<String> JAVA_IDENTIFIER() {	return lib.JAVA_IDENTIFIER; }
	@Token public TokenReader<?> KLEENE() { return lib.MULT; }
	@Keyword public TokenReader<?> KEYWORD_LA() { return new KeywordScanner("LA"); }
	@Keyword public TokenReader<?> KEYWORD_TOKENS() { return new KeywordScanner("Tokens:"); }
	@Keyword public TokenReader<?> KEYWORD_Start() { return new KeywordScanner("Start:"); }
	@Keyword public TokenReader<?> KEYWORD_RULES() { return new KeywordScanner("Rules:"); }
	@Keyword public TokenReader<?> KEYWORD_SEPARATORS() { return new KeywordScanner("Separators:"); }
	@Token public TokenReader<String> JAVA_STRING_LITERAL() { return lib.JAVA_STRING_LITERAL; }
	
	public class Grammar01Parser extends Parser<GrammarParsing> {

		/*
		   	SimpleGrammar
   			Start: Rule
   			Rules:
		 */
		public Rule Grammar(final SetCmd<Grammar> setCmd) { return new Rule() { public PC pc() {
			final Grammar g = new Grammar();
			
			SetCmd<String> setGrammarName = new SetCmd<String>() {
				public void set(String value) { 
					g.name = value; }};
			
			SetCmd<String> setStartRule = new SetCmd<String>() {
					public void set(String value) { g.startRuleName = value; }};
				
			return 
				AND(
					TOKEN(JAVA_IDENTIFIER(), setGrammarName),
					TOKEN(KEYWORD_Start()), TOKEN(JAVA_IDENTIFIER(), setStartRule),
					TOKEN(KEYWORD_RULES()),
					GrammarRules(addResultSetter(g.grammarRules)),
					result(setCmd, g)
				);
		}};}		
		
		/*
		 	Rule -> NonTerminal "->" Expression (ReturnCode)?. 
		 */
		public Rule GrammarRules(final SetCmd<GrammarRule> resultAdder) { return new Rule() { public PC pc() {
			return 
				NFOLD(GrammarRule(resultAdder));
		}};}
		
		/*
			GrammarRule -> NonTerminal "->" Expression (ReturnCode)?. 
		*/
		public Rule GrammarRule(final SetCmd<GrammarRule> resultAdder) { return new Rule() { public PC pc() { 
			final GrammarRule rule = new GrammarRule();
			SetCmd<Expression> setRuleName = new SetCmd<Expression>() {
				public void set(Expression value) { 
//					System.out.println("ruleName: " + value);
					rule.ruleName = ((NonTerminal) value).name; }};
			SetCmd<Expression> setExpression = new SetCmd<Expression>() {
				public void set(Expression value) {
//					System.out.println("expression: " + value);
					rule.expression = value;}};

				return 
				AND(
					NON_TERMINAL(setRuleName),
					TOKEN("->"),
					EXPRESSION(setExpression),
					TOKEN('.'),
					result(resultAdder, rule)
				);
		}};}

		/**
		  	Expression -> SimpleExpression+ (("|" (SimpleExpression)+ )*. 
		 */
		public Rule EXPRESSION(final SetCmd<Expression> result) { return new Rule() { public PC pc() { 
			final ResultObject<Expression> resultObject = new ResultObject<Expression>();
			
			SetCmd<Expression> changeHierarchy = new SetCmd<Expression>() {
				public void set(Expression value) {
					syso("here");
					if (resultObject.getResult() instanceof Alternative) {
						((Alternative) resultObject.getResult()).expressions.add(value);
					} else {
						Expression ex = resultObject.getResult();
						resultObject.result = value;
						((Alternative) value).expressions.add(0, ex);
					}
				} 
			};
			
			return 
				AND(
					AND(
						CONCAT(resultObject.setter)
//						, OPTIONAL(RETURN_CODE())
						),
						OPTIONAL(ALTERNATIVES(changeHierarchy)
					),
					result(result, resultObject.getter)
				);
		}};}	
		
		public Rule ALTERNATIVES(final SetCmd<Expression> result) { return new Rule() { public PC pc() { 
			Alternative alternative = new Alternative();
			SetCmd<Expression> alternativeAdder = addResultSetter(alternative.expressions);
			return 
				AND(
				  NFOLD(
					ALTERNATIVE(alternativeAdder)
				  ),
				  result(result, alternative)
				);
		}};}		
		
		public Rule ALTERNATIVE(final SetCmd<Expression> result) { return new Rule() { public PC pc() {
			ResultObject<Expression> res = new ResultObject<Expression>();
			return 
				AND(
					TOKEN('|'),
//					AND(
					CONCAT(res.setter),
//						, OPTIONAL(RETURN_CODE()))
					result(result, res.getter)
				);
		}};}			

		public Rule CONCAT(final SetCmd<Expression> resultSetter) { return new Rule() { public PC pc() { 
			final Concatenation concat = new Concatenation();
			SetCmd<Expression> addResults = addResultSetter(concat.expressions);
			
			// If Concat has only one element, then set element as result
			SetCmd<Expression> doResultSetting = new SetCmd<Expression>() {
				public void set(Expression value) {
					if (concat.expressions.size()==1) {
						resultSetter.set(concat.expressions.get(0));

					} else {
						resultSetter.set(concat);
					}
				}
			};
			
			return 
				AND(
					NFOLD(SIMPLE_EXPRESSION(addResults)),
					result(doResultSetting, concat)
				);
		}};}		
		
		
		
		/*
		 * 	SimpleExpression -> (BracketExpression | Terminal | NonTerminal) ("?" | "*")?.
		 */
		public Rule SIMPLE_EXPRESSION(final SetCmd<Expression> result) { return new Rule() { public PC pc() {
			final ResultObject<Expression> resultObject = new ResultObject<Expression>();
			SetCmd<UnaryOperator> pushUp = new SetCmd<UnaryOperator>() {
				public void set(UnaryOperator value) {
					Expression r = resultObject.getResult();
					resultObject.result = value;
					value.expression = r;
				}
			};
			
			return 
				AND(
					SIMPLE_ATOMICEXPRESSION(resultObject.setter),
//					OR(
//						AND(OPTIONAL(VARBINDING()), SIMPLE_ATOMICEXPRESSION(resultObject.setter))
//						, SIMPLE_ATOMICEXPRESSION(resultObject.setter)
//					),
					OPTIONAL(
						OR(
							AND(TOKEN('?'), result(pushUp, new Optional())),
							AND(TOKEN('*'), result(pushUp, new MultipleStar())), 
							AND(TOKEN('+'), result(pushUp, new MultiplePlus())))), 
					result(result, resultObject.getter)
				);
		}};}
		
//		public Rule OPTIONAL(final SetCmd<Expression> set) { return new Rule() { public PC pc() {
//			Optional optional = new Optional();
//			
//			return 
//					TOKEN('?');
//		}};}			
	
		
		/*
		 * 	SimpleExpression -> (BracketExpression | Terminal | NonTerminal) ("?" | "*")?.
		 */
		public Rule SIMPLE_ATOMICEXPRESSION(final SetCmd<Expression> setCmd) { return new Rule() { public PC pc() { 
			final ResultObject<Expression> result = new ResultObject<Expression>();
			
			return 
				AND(
				  OR(BRACKET_EXPRESSION(result.setter), TERMINAL(result.setter), NON_TERMINAL(result.setter)),
				  result(setCmd, result.getter)
				);
		}};}	
		
//		/*
//		 * 	SimpleExpression -> (BracketExpression | Terminal | NonTerminal) ("?" | "*")?.
//		 */
//		public Rule VARBINDING() { return new Rule() { public PC pc() { 
//			return 
//				AND(TOKEN(JAVA_IDENTIFIER()), TOKEN('='));
//		}};}		

		/*
		 *  			BracketExpression -> "(" Expression ")".
		 */
		public Rule BRACKET_EXPRESSION(final SetCmd<Expression> result) { return new Rule() { public PC pc() { 
			final BracketExpression bracketExpression = new BracketExpression();
			
			SetCmd<Expression> setExpression = new SetCmd<Expression>() {
				public void set(Expression value) {
					bracketExpression.expression = value;
				}
			};
			
			return 
				AND(
					TOKEN('('),
					EXPRESSION(setExpression),
					TOKEN(')'),
					result(result, bracketExpression)
				);
		}};}		

		
		public Rule NON_TERMINAL(final SetCmd<Expression> result) { return new Rule() { public PC pc() {
			final NonTerminal resultObject = new NonTerminal();
			SetCmd<String> setName = new SetCmd<String>() {
				public void set(String value) {
					resultObject.name = value;
				}
			};
			return 
				AND(
					TOKEN(JAVA_IDENTIFIER(), setName),
					result(result, resultObject)
				);
		}};}	
		
		
		/*
		 *  			Terminal -> "\"" Identifier "\"".
		 */		
		public Rule TERMINAL(final SetCmd<Expression> set) {return new Rule() { public PC pc() { 
			
			final Terminal t = new StringTerminal();
			SetCmd<String> setName = new SetCmd<String>() {public void set(String value) {t.value = value;}};

			return 
				AND(
				  TOKEN(JAVA_STRING_LITERAL(), setName),
				  result(set, t)
				);
		}};}	

		/*
		ReturnCode -> "{" "^" (ObjectReturn | LAReturn) "}".
		
		*/
//		public Rule RETURN_CODE() { return new Rule() { public PC pc() { 
//			return 
//				AND(
//					TOKEN('{'),
//					TOKEN('^'),
//					OR(OBJECT_RETURN(), LA_RETURN()),
//					TOKEN('}')
//				);
//		}};}	

/*
 	ObjektReturn -> TypeName (PropertyValue)+.
 */
		public Rule OBJECT_RETURN() { return new Rule() { public PC pc() { 
			return 
				AND(
					TOKEN(JAVA_IDENTIFIER()),
					NFOLD(PROPERTY_VALUE())
				);
		}};}		
				
		/*
 	PropertyValue -> propertyName ":" localVariable
	 */
		public Rule PROPERTY_VALUE() { return new Rule() { public PC pc() { 
			return 
				AND(
					TOKEN(JAVA_IDENTIFIER()),
					TOKEN(':'),
					TOKEN(JAVA_IDENTIFIER())
				);
		}};}	
		
		/*
			LAReturn -> "LA" "(" identifier "," identifier "(" identifier identifier ")" ")".		
	 	parseRule("AStupidRule -> a=anotherRule+ b=xRule {^LA(a, MyType(left,right)}.");
	 */
			public Rule LA_RETURN() { return new Rule() { public PC pc() { 
				return 
					AND(
						TOKEN("LA"), TOKEN('('), 
						TOKEN(JAVA_IDENTIFIER()),
						TOKEN(','),
						TOKEN(JAVA_IDENTIFIER()),
						TOKEN('('),
						TOKEN(JAVA_IDENTIFIER()),
						TOKEN(','),
						TOKEN(JAVA_IDENTIFIER()),
						TOKEN(')'),
						TOKEN(')')
					);
			}};}		

	}


}

package slope.lib.grammar;

import slope.Parsing;
import slope.ResultObject;
import slope.SetCmd;
import slope.lib.grammar.GrammarParsingWithTokens.GrammarWithTokensParser;
import slope.lib.grammar.parsetree.Grammar;
import slope.lib.grammar.parsetree.Token;
 
public class DynamicGrammarParserFactory {
	public DynamicGrammarParser createParserFromString(String grammarString) {
//System.out.println("dynamic parser 1: " + grammarString);		
		GrammarParsingWithTokens tokens = new GrammarParsingWithTokens();
		Parsing<GrammarParsingWithTokens> p = new Parsing<GrammarParsingWithTokens>(tokens, grammarString); 
		
		ResultObject<Grammar> grammarResult = new ResultObject<Grammar>();
		p.parseWithParseTreeConstruction(tokens.new GrammarWithTokensParser().Grammar(grammarResult.setter));
		Grammar grammar = grammarResult.result;
		
		DynamicGrammarParser dynamicParser = grammar.createParser();

//		System.out.println("TOKENS: ");		
//		for (String t: dynamicParser.tokens.keySet()) {		
//			System.out.println(t);	
//		}		
		
		return dynamicParser;
	}
	
//	public DynamicGrammarParser createParserWithResultFromString(String grammarString, SetCmd result) {
////System.out.println("dynamic parser: " + grammarString);		
//		GrammarParsingWithTokens tokens = new GrammarParsingWithTokens();
//		Parsing<GrammarParsingWithTokens> p = new Parsing<GrammarParsingWithTokens>(tokens, grammarString); 
//
//		ResultObject<Grammar> grammarResult = new ResultObject<Grammar>();
//		p.parseWithParseTreeConstruction(tokens.new GrammarWithTokensParser().Grammar(grammarResult.setter));
//		Grammar grammar = grammarResult.result;
//		return grammar.createParser();
//	}	
}

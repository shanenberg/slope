package slope.lib.grammar.parsetree;

import slope.lib.regex.RegExScanner;
import slowscane.TokenType;
import slowscane.scanners.TokenReader;
import slowscane.streams.PositionStream;

public class TokenSkipped extends Token {
	
	public TokenReader<String> createTokenReader() {
		//System.out.println("REGEX: " + regExString);		
				RegExScanner r = new RegExScanner(regExString, name);
//				r.getTokenType().setTokenTypeName(name);
				return r;
	}
	
//	public class SkippedRegEx extends RegExScanner {
//
//		public SkippedRegEx(String regex, String name) {
//			super(regex, name);
//		}
//		
//		public slowscane.Token<String> scanNext(PositionStream<?> stream) {
//			
//			System.out.println("scanNext: " + regex + " " + name);
//			
//			return super.scanNext(stream);
//		}
//		
//		protected boolean tokenIsOfRightKind(slowscane.Token token) {
//			System.out.println("tokenIsRight: " + token);			
//			System.out.println("tokenIsRight: " + token.getTokenType().equals(getTokenType()));			
////			System.out.println("REGEX Check: " + this.getTokenType());		
////			System.out.println("REGEX Check: " + token.getTokenType());		
//			return token.getTokenType().equals(getTokenType());
//		}			
//		
//	}
	
}

package slope.lib.grammar.parsetree;

import java.util.ArrayList;
import java.util.Iterator;

import slope.Parsing;
import slope.ResultObject;
import slope.SetCmd;
import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.GrammarParsing;
import slope.lib.grammar.GrammarParsingWithTokens;
import slope.lib.grammar.GrammarParsing.Grammar01Parser;
import slope.lib.grammar.parsetree.visitors.GrammarRuleVisitor;
import slope.lib.grammar.parsetree.visitors.RegisterTerminalVisitor;
import slowscane.Scanner;
import slowscane.scanners.TokenReader;

public class Grammar  {
	public String name;
	public String startRuleName;
	public ArrayList<Token> tokens = new ArrayList<Token>();
	public ArrayList<GrammarRule> grammarRules = new ArrayList<GrammarRule>();
	
	public DynamicGrammarParser createParser() {
		 
		DynamicGrammarParser parser = new DynamicGrammarParser(this);
		
		for (Token token : tokens) {
			if (!(token instanceof TokenSkipped)) {
				parser.addTokenIfNotKnownBack(token.name, token.createTokenReader());
			} else {
				parser.addSkippedToken(token.name, token.createTokenReader());
			}
		}

		for (GrammarRule grammarRule : grammarRules) {
			parser.addRule(grammarRule.ruleName, grammarRule);
			grammarRule.accept(new RegisterTerminalVisitor(parser));			
		}
		
		return parser;
	}		
	
	public Token getTokenNamed(String tokenName) {
		for (Token token : tokens) {
			if (token.name.equals(tokenName))
				return token;
		}
		throw new RuntimeException("Unknown token in Grammar " + name + "..." + tokenName);
	}
	
	
}

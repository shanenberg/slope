package slope.lib.grammar.parsetree.visitors;

import slope.Rule;
import slope.SetCmd;
import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.parsetree.Alternative;
import slope.lib.grammar.parsetree.BracketExpression;
import slope.lib.grammar.parsetree.BuiltInTerminal;
import slope.lib.grammar.parsetree.Concatenation;
import slope.lib.grammar.parsetree.Expression;
import slope.lib.grammar.parsetree.LibraryTerminal;
import slope.lib.grammar.parsetree.MultiplePlus;
import slope.lib.grammar.parsetree.MultipleStar;
import slope.lib.grammar.parsetree.NonTerminal;
import slope.lib.grammar.parsetree.Optional;
import slope.lib.grammar.parsetree.ReturnCode;
import slope.lib.grammar.parsetree.StringTerminal;
import slope.lib.grammar.parsetree.Terminal;
import slope.lib.grammar.parsetree.Token;
import slope.lib.grammar.parsetree.TokenRefTerminal;
import slope.lib.grammar.parsetree.TokenSkipped;

public interface GrammarRuleVisitor<RETURN_TYPE> {
	public RETURN_TYPE visit(Alternative a);
	public RETURN_TYPE visit(BuiltInTerminal a);
	public RETURN_TYPE visit(BracketExpression e);
	public RETURN_TYPE visit(Concatenation e);
	public RETURN_TYPE visit(LibraryTerminal e);
	public RETURN_TYPE visit(MultiplePlus e);
	public RETURN_TYPE visit(MultipleStar e);
	public RETURN_TYPE visit(NonTerminal e);
	public RETURN_TYPE visit(Optional e);
//	public RETURN_TYPE visit(ReturnCode e);
	public RETURN_TYPE visit(StringTerminal e);
	public RETURN_TYPE visit(TokenRefTerminal e);
}

package slope.lib.grammar.parsetree.visitors;

import java.util.ArrayList;

import slope.PC;
import slope.Rule;
import slope.SetCmd;
import slope.lang.SET.SYSOParser;
import slope.lib.SlopeLib;
import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.parsetree.Alternative;
import slope.lib.grammar.parsetree.BracketExpression;
import slope.lib.grammar.parsetree.BuiltInTerminal;
import slope.lib.grammar.parsetree.Concatenation;
import slope.lib.grammar.parsetree.Expression;
import slope.lib.grammar.parsetree.LibraryTerminal;
import slope.lib.grammar.parsetree.MultiplePlus;
import slope.lib.grammar.parsetree.MultipleStar;
import slope.lib.grammar.parsetree.NonTerminal;
import slope.lib.grammar.parsetree.Optional;
import slope.lib.grammar.parsetree.ReturnCode;
import slope.lib.grammar.parsetree.StringTerminal;
import slope.lib.grammar.parsetree.Terminal;
import slope.lib.grammar.parsetree.TokenRefTerminal;
import slope.lib.grammar.parsetree.TokenSkipped;
import slope.lib.regex.RegExScanner;
import slowscane.scanners.TokenReader;

public class CreateParserRuleVisitor implements GrammarRuleVisitor<Rule> {
	
	final SetCmd resultSetter;
	final DynamicGrammarParser dynamicParser;
	
	public CreateParserRuleVisitor(SetCmd resultSetter, DynamicGrammarParser p) {
		super();
		this.resultSetter = resultSetter;
		this.dynamicParser = p;
	}

	public Rule visit(final Alternative e) {
		return new Rule(){ public PC pc() {
			final Alternative result = new Alternative();
			final SetCmd setter = dynamicParser.addResultSetter(result.expressions);

			final CreateParserRuleVisitor createParserRuleVisitor = 
					new CreateParserRuleVisitor(setter, dynamicParser);

			final ArrayList<PC> expressionParsers = new ArrayList<PC>();
			
			for (Expression expression : e.expressions) {
				expressionParsers.add(expression.acceptVisitor(createParserRuleVisitor));
			}		
			
				return 
					dynamicParser.AND(
						dynamicParser.OR(expressionParsers.toArray(new Rule[expressionParsers.size()])),
						dynamicParser.result(resultSetter, result));
		}}; 
	}

	public Rule visit(final BracketExpression e) {

		return new Rule(){ public PC pc() {

			final BracketExpression result = new BracketExpression();
			final SetCmd setExpression = new SetCmd<Expression>() { public void set(Expression value) {result.expression = value;} };
			
			final CreateParserRuleVisitor createParserRuleVisitor = 
					new CreateParserRuleVisitor(setExpression, dynamicParser);

			final ArrayList<PC> expressionParsers = new ArrayList<PC>();
			
			final Rule expressionPC = e.expression.acceptVisitor(createParserRuleVisitor);

			return dynamicParser.AND(
				expressionPC, 
				dynamicParser.result(resultSetter, result));
		}};
	}

	public Rule visit(final Concatenation e) {
		return new Rule(){ public PC pc() {

			final Concatenation result = new Concatenation();
			final ArrayList<PC> expressionParsers = new ArrayList<PC>();
			final SetCmd addExpressionCmd = dynamicParser.addResultSetter(result.expressions);
			
			final CreateParserRuleVisitor createParserRuleVisitor = 
					new CreateParserRuleVisitor(addExpressionCmd, dynamicParser);
			
			for (Expression expression : e.expressions) {
				expressionParsers.add(expression.acceptVisitor(createParserRuleVisitor));
			}		
			
				return dynamicParser.AND(
					dynamicParser.AND(expressionParsers.toArray(new Rule[expressionParsers.size()])),
					dynamicParser.result(resultSetter, result));
		}};
	}

	public Rule visit(final MultiplePlus e) {
//System.out.println("DID MULTIPLE PLUS");		
		return new Rule(){ public PC pc() {

			final MultiplePlus result = new MultiplePlus();
			result.expression = e.expression;
//			final SetCmd setter = new SetCmd<Expression>() { public void set(Expression value) { result.expression = value;	} };
			SetCmd expressionAdder = dynamicParser.addResultSetter(result.values);
			
			final CreateParserRuleVisitor createParserRuleVisitor = 
					new CreateParserRuleVisitor(expressionAdder, dynamicParser);
			
			final PC expressionParser = e.expression.acceptVisitor(createParserRuleVisitor);
			
				return dynamicParser.AND(
					dynamicParser.NFOLD(expressionParser),
					dynamicParser.result(resultSetter, result));
		}};
	}

	public Rule visit(final MultipleStar e) {
//System.out.println("DID MULTIPLE STAR");				
		return new Rule(){ public PC pc() {

			final MultipleStar result = new MultipleStar();
			result.expression = e.expression;
			SetCmd expressionAdder = dynamicParser.addResultSetter(result.values);
			
			final CreateParserRuleVisitor createParserRuleVisitor = 
					new CreateParserRuleVisitor(expressionAdder, dynamicParser);

			final PC expressionParser = e.expression.acceptVisitor(createParserRuleVisitor);
			
			return dynamicParser.AND(
					dynamicParser.NFOLD0(expressionParser),
					dynamicParser.result(resultSetter, result));
	}};		
	}

	public Rule visit(final Optional e) {
//System.out.println("DID OPTIONAL");		
		return new Rule() {
			public PC pc() {

				final Optional result = new Optional();
				final SetCmd setter = new SetCmd<Expression>() { 
					public void set(Expression value) { result.expression = value;	} };

				final CreateParserRuleVisitor createParserRuleVisitor = 
							new CreateParserRuleVisitor(setter, dynamicParser);
					
				return 
					dynamicParser.AND(
						dynamicParser.OPTIONAL(e.expression.acceptVisitor(createParserRuleVisitor)),
						dynamicParser.result(resultSetter, result));
			}
		};	
	}	

	public Rule visit(final NonTerminal e) {
		return new Rule() {
			public PC pc() {
				final NonTerminal result = new NonTerminal();
				result.name = e.name;
				SetCmd resultResult = new SetCmd<Expression>() {
					public void set(Expression value) {
						result.result = value;
					}
				};
//				System.out.println("e.name: " + e.name);	
//				System.out.println("1: " + resultResult);	
//				System.out.println("2: " + dynamicParser);	
//				System.out.println("3: " + result);	
//				System.out.println("3: " + resultSetter);	
//				System.out.println(dynamicParser.getRuleNamed(e.name));
					
				return dynamicParser.AND(
						dynamicParser.getRuleNamed(e.name).createParserRule(dynamicParser, resultResult), 
						dynamicParser.result(resultSetter, result));
			}
		};
	}
//
//	public Rule visit(ReturnCode e) {
//		throw new RuntimeException("Should be deleted...");
//	}
	
	public Rule visit(final LibraryTerminal e) {

		return new Rule() {
			public PC pc() {
				final LibraryTerminal result = new LibraryTerminal();
				result.libraryName = e.libraryName;
				result.libTokenName = e.libTokenName;
				SetCmd setter = new SetCmd<String>() {
					public void set(String value) {
						result.value = value;	
					}
				};
			
				
				return 
					dynamicParser.AND(
						dynamicParser.TOKEN(e.getTokenReaderFromDynamicParser(dynamicParser), setter),
						dynamicParser.result(resultSetter, result) // hier passiert Problem
				);
			}
		};
	}	
	
	public Rule visit(final StringTerminal e) {
				return new Rule(){ public PC pc() {

					final StringTerminal result = new StringTerminal();
					SetCmd setter = new SetCmd<String>() {
						public void set(String value) {
							result.value = value;	
						}
					};
					
					return dynamicParser.AND(
						dynamicParser.TOKEN(e.uncodedName(), setter),
						dynamicParser.result(resultSetter, result)
					);
				}};
	}	

	public Rule visit(final TokenRefTerminal e) {
		return new Rule() {
			public PC pc() {
//System.out.println("TOKENREF NAME: " + e);				
				final TokenRefTerminal result = new TokenRefTerminal();
				
				return 
						dynamicParser.AND(
								dynamicParser.TOKEN(dynamicParser.getTokenReaderNamed(e.name)),
								dynamicParser.result(resultSetter, result)
						);
			}
		};			
	}
	
	public Rule visit(final BuiltInTerminal e) {
		return new Rule() {
			public PC pc() {
				final TokenReader r = e.createTokenReader();	
				final BuiltInTerminal result = new BuiltInTerminal();
				result.terminalName = e.terminalName;
				final SetCmd setter = new SetCmd<String>() {
					public void set(String value) {
						result.value = value;	
					}
				};

				return dynamicParser.AND(
					dynamicParser.TOKEN(r, setter),
					dynamicParser.result(resultSetter, result));
			}
		};			
	}	

}

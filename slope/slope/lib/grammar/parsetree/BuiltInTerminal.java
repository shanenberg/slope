package slope.lib.grammar.parsetree;

import org.omg.Messaging.SyncScopeHelper;

import slope.Rule;
import slope.SetCmd;
import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.parsetree.visitors.GrammarRuleVisitor;
import slowscane.scanners.KeywordScanner;
import slowscane.scanners.TokenReader;
import slowscane.scanners.lib.JavaCharLiteralScanner;
import slowscane.scanners.lib.JavaStringLiteralScanner;

public class BuiltInTerminal extends Terminal {
	public String terminalName;

	@Override
	public void printTo(StringBuffer buffer) {
		buffer.append("<#" + terminalName + "#>");
	}
	
	public TokenReader<String> createTokenReader() {
		if (terminalName.equals("StringLiteral")) {
			JavaStringLiteralScanner st = new JavaStringLiteralScanner();
			return st;
		}
		
		if (terminalName.equals("CharacterLiteral")) {
			JavaCharLiteralScanner st = new JavaCharLiteralScanner();
			return st;
		}
		
		
		System.err.println("NO StringLiteral APPEARED!");
		throw new RuntimeException("Only STRINGTERMINAL current supported");
	}
	 
	public <RETURN_TYPE> RETURN_TYPE acceptVisitor(GrammarRuleVisitor<RETURN_TYPE> visitor) {
		return visitor.visit(this);
	}		
}

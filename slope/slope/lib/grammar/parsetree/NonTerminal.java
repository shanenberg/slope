package slope.lib.grammar.parsetree;

import slope.PC;
import slope.Rule;
import slope.SetCmd;
import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.parsetree.visitors.GrammarRuleVisitor;

public class NonTerminal extends Expression {
	public String name;

	@Override
	public void printTo(StringBuffer buffer) {
		buffer.append(name);
	}

	public <RETURN_TYPE> RETURN_TYPE acceptVisitor(GrammarRuleVisitor<RETURN_TYPE> visitor) {
		return visitor.visit(this);
	}
	
	public Expression result;
}

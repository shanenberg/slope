package slope.lib.grammar.parsetree;

import slope.lib.regex.RegExScanner;
import slowscane.scanners.TokenReader;

public class Token {
	public String name;
	public String regExString;
	
	public TokenReader<String> createTokenReader() {
//System.out.println("REGEX: " + regExString);		
		RegExScanner r = new RegExScanner(regExString, name);
//		r.getTokenType().setTokenTypeName(name);
		return r;
	}
}

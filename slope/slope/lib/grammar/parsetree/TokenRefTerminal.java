package slope.lib.grammar.parsetree;

import slope.Rule;
import slope.SetCmd;
import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.parsetree.visitors.GrammarRuleVisitor;
import slowscane.scanners.TokenReader;

public class TokenRefTerminal extends Terminal {
	public String name;

	@Override
	public void printTo(StringBuffer buffer) {
		buffer.append("\"" + name + "\"");
	}
	
	public <RETURN_TYPE> RETURN_TYPE acceptVisitor(GrammarRuleVisitor<RETURN_TYPE> visitor) {
		return visitor.visit(this);
	}

	@Override
	public TokenReader<String> createTokenReader() {
		// TODO Auto-generated method stub
		return null;
	}		
}

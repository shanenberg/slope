package slope.lib.grammar.parsetree;

import java.util.ArrayList;

import slope.PC;
import slope.Parser;
import slope.Rule;
import slope.SetCmd;
import slope.lib.SlopeLib;
import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.parsetree.visitors.GrammarRuleVisitor;
import slowscane.scanners.TokenReader;

public class LibraryTerminal extends Terminal {
	public String libraryName;
	public String libTokenName;

	@Override
	public void printTo(StringBuffer buffer) {
		buffer.append("<" + libraryName + " "+ libTokenName + ">");
	}

	public <RETURN_TYPE> RETURN_TYPE acceptVisitor(GrammarRuleVisitor<RETURN_TYPE> visitor) {
		return visitor.visit(this);
	}		

	@Override
	public TokenReader createTokenReader() {
		return SlopeLib.getInstance().getTokenReaderFromLib(libraryName, libTokenName);
	}
	
	public TokenReader getTokenReaderFromDynamicParser(DynamicGrammarParser dynamicParser) {
		return dynamicParser.getTokenReaderNamed(libraryName + "/" + libTokenName);
	}
	
	public void registerTokenReadertoDynamicParser(DynamicGrammarParser dynamicParser) {
		dynamicParser.addTokenIfNotKnownBack(libraryName + "/" + libTokenName, createTokenReader());
	}	


}

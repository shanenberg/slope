package slope.lib.grammar.parsetree;

import slope.Parser;
import slope.Rule;
import slope.SetCmd;
import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.parsetree.visitors.GrammarRuleVisitor;

public class Optional extends UnaryOperator {

	@Override
	public void printTo(StringBuffer buffer) {
		if (expression!=null)
			expression.printTo(buffer);
		buffer.append("?");
	}

	public <RETURN_TYPE> RETURN_TYPE acceptVisitor(GrammarRuleVisitor<RETURN_TYPE> visitor) {
		return visitor.visit(this);
	}		
}

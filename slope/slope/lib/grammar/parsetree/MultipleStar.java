package slope.lib.grammar.parsetree;

import java.util.ArrayList;

import slope.Parser;
import slope.Rule;
import slope.SetCmd;
import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.parsetree.visitors.GrammarRuleVisitor;

public class MultipleStar extends UnaryOperator{

	public ArrayList<Expression> values = new ArrayList<Expression>();

	@Override
	public void printTo(StringBuffer buffer) {
		expression.printTo(buffer);
		buffer.append("*");
	}

	public <RETURN_TYPE> RETURN_TYPE acceptVisitor(GrammarRuleVisitor<RETURN_TYPE> visitor) {
		return visitor.visit(this);
	}		
}

package slope.lib.grammar.parsetree;

import slope.PC;
import slope.Parser;
import slope.Rule;
import slope.SetCmd;
import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.parsetree.visitors.GrammarRuleVisitor;
import slowscane.scanners.KeywordScanner;
import slowscane.scanners.TokenReader;

/*
 * A String terminal is a terminal symbol starting and closing with "
 * Example: "myKeyword"
 * 
 * the whole String (including starting and ending ") is stored in name.
 */
public class StringTerminal extends Terminal {

	public String uncodedName() {
		return value.substring(1, value.length()-1);
	}
	
	@Override
	public void printTo(StringBuffer buffer) {
		buffer.append(" " + value + " ");
	}

	public TokenReader<String> createTokenReader() {
		return new KeywordScanner(uncodedName());
	}

	public <RETURN_TYPE> RETURN_TYPE acceptVisitor(GrammarRuleVisitor<RETURN_TYPE> visitor) {
		return visitor.visit(this);
	}			
}

package slope.lib.grammar;

import java.util.ArrayList;

public class DynamicTerminal {
	
	public final String typeName;
	
	public final ArrayList<DynamicTerminal> children = new ArrayList<DynamicTerminal>();
	
	public DynamicTerminal(String typeName) {
		super();
		this.typeName = typeName;
	}

	@Override
	public String toString() {
		StringBuffer ret = new StringBuffer();

		ret.append("DynamicTreeNode: " + typeName);
		
		for (DynamicTerminal dynamicTreeNode : children) {
			ret.append(dynamicTreeNode.toString());
		}
		
		return ret.toString();
	}
	
}

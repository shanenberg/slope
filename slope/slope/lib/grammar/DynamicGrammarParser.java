package slope.lib.grammar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import shapuc.util.Pair;
import slope.ElementParser;
import slope.PC;
import slope.Parser;
import slope.Parsing;
import slope.ResultObject;
import slope.RootElementParser;
import slope.RootElementTreeNode;
import slope.Rule;
import slope.SetCmd;
import slope.TreeNode;
import slope.lib.grammar.GrammarParsing.Grammar01Parser;
import slope.lib.grammar.parsetree.Grammar;
import slope.lib.grammar.parsetree.GrammarRule;
import slope.lib.regex.RegExScanner;
import slope.treenodecompression.RunSetObjects;
import slowscane.Scanner;
import slowscane.scanners.TokenReader;
import slowscane.streams.PositionStream;
import slowscane.streams.StringBufferStream;
import slowscane.streams.TokenStream;

public class DynamicGrammarParser extends Parser {

	ArrayList<Pair<String, TokenReader>> tokens = new ArrayList<Pair<String, TokenReader>>();
	ArrayList<Pair<String, TokenReader>> skippedTokens = new ArrayList<Pair<String, TokenReader>>();
	
//	HashMap<String, TokenReader> skippedTokens = new HashMap<String, TokenReader>();
	HashMap<String, GrammarRule> rules = new HashMap<String, GrammarRule>();

	final Grammar grammar;

	public DynamicGrammarParser(Grammar grammar) {
		this.grammar = grammar;
	}
	
	public boolean containsKey(ArrayList<Pair<String, TokenReader>> list, String key) {
		for (Pair<String, TokenReader> pair : list) {
			if (pair.getLeft().equals(key))
				return true;
		}
		return false;
	}

	public void putAsPairFront(ArrayList<Pair<String, TokenReader>> list, String key, TokenReader r) {
		list.add(0, new Pair<String, TokenReader>(key, r));
	}

	public void putAsPairBack(ArrayList<Pair<String, TokenReader>> list, String key, TokenReader r) {
		list.add(new Pair<String, TokenReader>(key, r));
	}
	
	public void addTokenIfNotKnownFront(String token, TokenReader tokenReader) {
		if (containsKey(tokens, token)) return;
		
		if (tokenReader instanceof RegExScanner) {
			((RegExScanner) tokenReader).getTokenType().setTokenTypeName(token);;
		}
		putAsPairFront(tokens, token, tokenReader);
	}	
	
	public void addTokenIfNotKnownBack(String token, TokenReader tokenReader) {
		if (containsKey(tokens, token)) return;
		
		if (tokenReader instanceof RegExScanner) {
			((RegExScanner) tokenReader).getTokenType().setTokenTypeName(token);;
		}
		putAsPairBack(tokens, token, tokenReader);
	}
	
	public TokenReader getTokenReaderNamed(String tokenName) {
		for (Pair<String, TokenReader> pair : tokens) {
			if (pair.getLeft().equals(tokenName))
				return pair.getRight();
		}
		throw new RuntimeException("Not contained");
	}	

	public void addRule(String ruleName, GrammarRule rule) {
		rules.put(ruleName, rule);
	}

	public GrammarRule getRuleNamed(String ruleName) {
		return rules.get(ruleName);
	}
	
	public TreeNode parseWithResultObjectCreation(String word, String ruleName, SetCmd result) {
		TreeNode tn = this.parse(word, ruleName, result);
		RunSetObjects bindObject = new RunSetObjects();
		tn.accept(bindObject);
		return tn;
	}
	
	public TreeNode parseWithResultObjectCreation(String word, SetCmd result) {
		TreeNode tn = this.parse(word, grammar.startRuleName, result);
		RunSetObjects bindObject = new RunSetObjects();
		tn.accept(bindObject);
		return tn;
	}	

	public TreeNode parseWithResultObjectCreation(String word) {
		TreeNode tn = this.parse(word);
		RunSetObjects bindObject = new RunSetObjects();
		tn.accept(bindObject);
		return tn;
	}
	
	public TreeNode parse(String word, String ruleName, SetCmd result) {
		PositionStream stream = new StringBufferStream(word);
		
		
		
		Scanner scanner = new Scanner(getTokens(), getSkippedTokens(), stream);

		scanner.setPositionStream(stream);
		TokenStream tokenStream = scanner.scan();
//System.out.println("RUleName: " + ruleName);
//System.out.println();
		Rule r = this.getRuleNamed(ruleName).createParserRule(this, result);
		RootElementParser parser = r.createParser();

		RootElementTreeNode ret = parser.parse(tokenStream);
		
		if (!tokenStream.isEOF())
			throw parser.createParseException(tokenStream, "Haven't reached EOF.");

		return ret;
	}

	public TreeNode parse(String word, SetCmd result) {
		return this.parse(word, grammar.startRuleName, result);
	}
	
	public TreeNode parse(String word) {
		return this.parse(word, grammar.startRuleName, new ResultObject().setter);
	}	

	public List<TokenReader> getTokens() {
		ArrayList ret = new ArrayList();
		for (Pair<String, TokenReader> pair : tokens) {
			ret.add(pair.getRight());
		}
		return ret;
	}

	public List<TokenReader> getSkippedTokens() {
		ArrayList ret = new ArrayList();
		for (Pair<String, TokenReader> pair : skippedTokens) {
			ret.add(pair.getRight());
		}
		return ret;
	}


	public void addSkippedToken(String name, TokenReader<String> tokenReader) {
		putAsPairBack(skippedTokens, name, tokenReader);
	}

}

/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope.lib.automata.ext;

import javax.management.RuntimeErrorException;

import slope.Cmd;
import slope.PC;
import slope.Parser;
import slope.Rule;
import slope.SetCmd;
import slowscane.Tokens;
import slowscane.annotations.Keyword;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.scanners.TokenReader;

/**
 * 

	Start ->  Automataname Rules "."
	Rules -> Rule ("," Rule)*
	Rule -> <IDENTIFIER "-->" Transition "-->" NextState
	Transition -> (InMSG | "ε") "/" (IN_Condition| "ε") "/" (OUT_STATEMENT | "ε")
	NextState -> <NEXTSTATE> ("/" <NEXZ_STATECMD>"
 *
 */		

public class AutomataParsing extends Tokens {
	
	// Whitespaces are separators but not used while parsing (i.e. they are skipped)
	@Separator public TokenReader<?> WHITESPACE() {	return lib.WHITESPACE; 	}
	@Separator public TokenReader<?> NEWLINE() {	return lib.NEWLINE; 	}
	@Separator public TokenReader<?> TAB() {	return lib.TAB; 	}
	
	@Keyword public TokenReader<String> PACKAGE_KEYWORD() { return lib.KEYWORD_PACKAGE;}
	@Keyword public TokenReader<?> ARROW() { return lib.KEYWORD_LONG_ARROW;}
	@Keyword public TokenReader<?> EXCLMARK() { return lib.EXCLMARK;}
	
	@Token public TokenReader<?> SLASH() { return lib.DIV;}
	@Token public TokenReader<?> MINUS() { return lib.MINUS;}
	@Token public TokenReader<?> EPSILON() { return lib.EPSILON;}
	@Token public TokenReader<?> SEMICOLON() { return lib.SEMICOLON;}
	@Token public TokenReader<?> COMMA() { return lib.COMMA;}
	@Token public TokenReader<String> IDENTIFIER() { return lib.JAVA_IDENTIFIER;}
	@Token public TokenReader<?> DOT() { return lib.DOT;}
	@Token public TokenReader<?> OPEN_BRACKET() { return lib.OPEN_BRACKET; }
	@Token public TokenReader<?> CLOSED_BRACKET() { return lib.CLOSED_BRACKET; }
	
	public class AutomataParser extends Parser<AutomataParsing> {

		/*
		 * START -> PACKAGE, AUTOMATA_NAME_START, RULES  
		 */
		public Rule START(final SetCmd<Automata> result) {  return new Rule() { public PC pc() { 
			
			final Automata automata = new Automata();
			
			return
			  AND(
//			    PACKAGE(automata),
			    AUTOMATA_NAME_START(automata),
			    RULES(automata), TOKEN('.'),
			    set(setResult(result, automata))
			  );
		}};}

		/*
		 * <PACKAGE> -> "package", identifier, ";"  
		 */
//		public Rule PACKAGE(final Automata automata) { return new Rule() { public PC pc() { return
//		  AND(TOKEN(PACKAGE_KEYWORD()), PACKAGE_NAME(automata), TOKEN(';'));
//		}};}
		
		/*
		 * The first identifier is the name of the automata, the second one the name of the identifier.
		 * 
		 * <PACKAGE> -> Identifier "(" Identifier ")"  
		 */
		Rule AUTOMATA_NAME_START(final Automata automata) { return new Rule() { public PC pc() { 
			
				final SetCmd<String> setName = new SetCmd<String>() {public void set(String value) {automata.name = value;} };
				final SetCmd<String> setStartRuleName = new SetCmd<String>() {public void set(String value) {automata.startStateName = value;} };
				
				
				return
			  AND(
				TOKEN(IDENTIFIER(), setName), 
				TOKEN('('), 
				TOKEN(IDENTIFIER(), setStartRuleName), 
				TOKEN(')'));
		}};}
		
		/*
		 * The first identifier is the name of the automata, the second one the name of the identifier.
		 * 
		 * <PACKAGE> -> Identifier "(" Identifier ")"  
		 */
		public Rule RULES(final Automata automata) {  
			return new Rule() { public PC pc() { 
			final SetCmd<Transition> addTransition = addResultSetter(automata.transitions);
			return
		    WITH_OPTIONAL(RULE(addTransition), NFOLD(AND(TOKEN(','), RULE(addTransition))))
		  ;
		}};}
					
		/*
		 * The first identifier is the name of the automata, the second one the name of the identifier.
		 * 
		 * <PACKAGE> -> Identifier "(" Identifier ")"  
		 */			
		
	
		public Rule RULE(final SetCmd<Transition> resultSetter) {
			
			return new Rule() { public PC pc() { 
				
			final Transition transition = new Transition();
			final SetCmd<String> setStartStateName = new SetCmd<String>() {public void set(String value) {transition.startStateName = value;}};

			return
			AND(
			  TOKEN(IDENTIFIER(), setStartStateName), 
			  TOKEN(ARROW()), 
			  TRANSITION(transition),
			  TOKEN(ARROW()), 
			  NEXT_STATE(transition),
			  set(setResult(resultSetter, transition))
			);
		}};}
		
		/*
		 * The first identifier is the name of the automata, the second one the name of the identifier.
		 * 
		 * <PACKAGE> -> Identifier "(" Identifier ")"  
		 */			
		

		
		public Rule TRANSITION(final Transition transition) { return new Rule() { public PC pc() { return
		  AND(
		    IN_MSG(transition), TOKEN('/'),
		    IN_CONDITION(transition), TOKEN('/'),
		    OUT_STATEMENT(transition)
		  );
		}};}
		
		public Rule IN_MSG(final Transition transition) { return new Rule() { public PC pc() { 

			
			final SetCmd<Character> setMessage = new SetCmd<Character>() {public void set(Character value) {transition.inMsg = value.toString();}};
			final SetCmd<String> setMessage2 = new SetCmd<String>() {public void set(String value) {transition.inMsg = value;}};
			
			return
			OR(
				TOKEN('ε', setMessage),
				TOKEN(IDENTIFIER(), setMessage2)
			);
		}};}
		
		public Rule IN_CONDITION(final Transition transition) {

			final SetCmd<Character> setMessage = new SetCmd<Character>() {public void set(Character value) {transition.conditionMsg = value.toString();}};
			final SetCmd<String> setMessage2 = new SetCmd<String>() {public void set(String value) {transition.conditionMsg = value;}};
			final SetCmd<String> setMessage3 = new SetCmd<String>() {public void set(String value) {transition.conditionMsg = "!" + value;}};
		
		return new Rule() { public PC pc() { return
		  OR(
		    AND(TOKEN('!'), TOKEN(IDENTIFIER(), setMessage3)), 
		    TOKEN('-', setMessage), 
		    TOKEN(IDENTIFIER(), setMessage2));
		}};}
		
		public Rule OUT_STATEMENT(final Transition transition) {
				final SetCmd<Character> setMessage = new SetCmd<Character>() {public void set(Character value) {transition.outMsg = value.toString();}};
				final SetCmd<String> setMessage2 = new SetCmd<String>() {public void set(String value) {transition.outMsg = value;}};
				
			return new Rule() {public PC pc() { return
				OR(
		    TOKEN('-', setMessage), 
		    TOKEN(IDENTIFIER(), setMessage2));
		}};}			
	
		
		public Rule NEXT_STATE(final Transition transition) {
			final SetCmd<String> setNextStateName = new SetCmd<String>() {public void set(String value) {transition.nextStateName = value;}};
			final SetCmd<String> setNextStateCmdName = new SetCmd<String>() {public void set(String value) {transition.nextStateCmdName = value;}};
			
			return new Rule() {public PC pc() { return
				WITH_OPTIONAL(
					TOKEN(IDENTIFIER(), setNextStateName), 
					AND(TOKEN('/'), TOKEN(IDENTIFIER(), setNextStateCmdName)));
		}};}			
		
		
//		public Rule PACKAGE_NAME(final Automata automata) { 
//		  final StringBuffer stringBuffer = new StringBuffer();
//		  final SetCmd<String> append = new SetCmd<String>() {public void set(String value) { stringBuffer.append(value);}};
//		  final SetCmd<Character> addPoint = new SetCmd<Character>() {public void set(Character value) { stringBuffer.append("."); }};
////		  final Cmd setResult = new Cmd() {public void doCmd() {automata.packageName = stringBuffer.toString();}};
//		  
//		  return new Rule() {public PC pc() { return
//		  AND(
//		    OR(
//			  AND(
//			    TOKEN(IDENTIFIER(), append), 
//			    NFOLD(
//			      AND(
//			        TOKEN('.', addPoint), 
//			        TOKEN(IDENTIFIER(), append)))),
//			  TOKEN(IDENTIFIER(), append)
//		    ),
//            set(setResult)
//          );
//		    
//		}};}	

	}
}		
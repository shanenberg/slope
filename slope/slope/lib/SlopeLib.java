package slope.lib;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import slope.Parsing;
import slope.ResultObject;
import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.GrammarParsingWithTokens;
import slope.lib.grammar.GrammarParsingWithTokens.GrammarWithTokensParser;
import slope.lib.grammar.parsetree.Grammar;
import slope.lib.grammar.parsetree.Token;
//TODO: I need to handle the libraries in a better way....
import slowscane.scanners.TokenReader;

public class SlopeLib {

	String libsPath = SlopeLib.class.getResource("libs") + File.separator;
	
	HashMap<String, Grammar> loadedLibs = new HashMap<String, Grammar>();
	
	static SlopeLib thisObject = new SlopeLib();
	
	public static SlopeLib getInstance() {return thisObject;}
	
	public void loadLibs() {
		for (String libName : libNames()) {
			loadLib(libName);
		}
	}
	
	public Grammar loadLib(String libName) {
		
		if (loadedLibs.containsKey(libName)) return loadedLibs.get(libName);
		
		try {
			InputStream aStream = getStreamFromLibName(libName);
			String libString = loadFileIntoString(aStream);
			ResultObject<Grammar> result = new ResultObject<Grammar>();
			GrammarParsingWithTokens tokens = new GrammarParsingWithTokens();
			Parsing<GrammarParsingWithTokens> p = new Parsing<GrammarParsingWithTokens>(tokens, libString);
			p.parseWithParseTreeConstruction(tokens.new GrammarWithTokensParser().Grammar(result.setter));
			loadedLibs.put(libName, result.getter.get());
			return result.result;

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public InputStream getStreamFromLibName(String libName) throws Exception {
		return getStreamFromFileName(libName + ".slopegrammar");
	}
	
	public InputStream getStreamFromFileName(String fileName) throws Exception {
		
		String pathString = libsPath + fileName;
		URL url =null;
		try {
			url = new URL(pathString);
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		return url.openStream();
	}	
	
	
	public String loadFileIntoString(InputStream aStream) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(aStream));
		
		String nextLine;
		boolean firstLine = true;
		StringBuffer result = new StringBuffer();
		while((nextLine = reader.readLine())!=null) {
			if (firstLine) {
				firstLine = false;
			} else {
				result.append("\n");
			}
			result.append(nextLine);
		}
		aStream.close();
		return result.toString();
	}	
	
	public ArrayList<String> grammarFileNamesWithoutPath() {
		ArrayList<String> ret = new ArrayList<String>();
		
		String grammarFolder = System.getProperty("user.dir") + File.separator + "examples" + File.separator;		
		File folder = new File(grammarFolder);
		
		File[] listOfFiles = folder.listFiles();
		
		for (File file : listOfFiles) {
		    if (file.isFile() && file.getName().endsWith(".slopegrammar")) {
		    	ret.add(file.getName());
		    }
		}		
		return ret;
	}
	
	public ArrayList<String> libNames() {
		ArrayList<String> libs = grammarFileNamesWithoutPath();
		ArrayList<String> ret = new ArrayList<String>();
		
		for (String lib: libs) {
			// name ends with .slops .... I remove is
			ret.add(lib.substring(0, lib.length()-6));
		}
		return ret;
	}
	
	public TokenReader<?> getTokenReaderFromLib(String libname, String tokenName) {
		
		if (!loadedLibs.containsKey(libname)) {
			this.loadLib(libname);
//			System.out.println("LOAD LIB" + libname);
			if (!this.loadedLibs.containsKey(libname))
				throw new RuntimeException("something really sucks");
		}
		
		for (Token t : this.loadedLibs.get(libname).tokens) {
			if (t.name.equals(tokenName))
				return t.createTokenReader();
		}
		
		throw new RuntimeException("Unknown token. Lib: " + libname + " tokenName: " + tokenName);
	}
	
	public void putLibIntoDynamicParser(DynamicGrammarParser dynamicParser, String libName, String tokenName) {
		System.out.println(-1);
		
		
	}
	
}

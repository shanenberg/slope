/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope;

import java.util.ArrayList;
import java.util.List;

public class TreeNodeImpl_N extends TreeNodeImpl {
	
	public TreeNodeImpl_N(String treeNodeName) {
		super(/*treeNodeName*/);
	}

	ArrayList<TreeNode> treeNodes = new ArrayList<TreeNode>();

	public void add(TreeNode node) {
//		if (!node.isSkipped())
			treeNodes.add(node);
	}
	
	public TreeNode get(int i) {
		return treeNodes.get(i);
	}
	
	public int size() {
		return treeNodes.size();
	}

//	public void addAll(NFoldTreeNode tmp) {
//		for (int i = 0; i < tmp.size(); i++) {
//			this.add((TreeNodeType) tmp.get(i));
//		}
//	}

	public List<TreeNode> getChildren() {
		return treeNodes;
	}		
	
	
	
}

/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope;

import java.util.ArrayList;
import java.util.List;

import shapuc.util.Reflection;

public class ParserConstructor_1<PARSER_TYPE extends ElementParser> implements PC {

	protected final Class<PARSER_TYPE> parserType;
	protected final PC pc;
	protected final String pcTypeName;
	
	public ParserConstructor_1(String pcTypeName, Class<PARSER_TYPE> treeNodeType, PC pc) {
		super();
		this.parserType = treeNodeType;
		this.pc = pc;
		this.pcTypeName = pcTypeName;
	}
	
	/** Creates the parser instance using Reflection */
	public PARSER_TYPE createParser() {
		return (PARSER_TYPE) Reflection.instantiateClassUnsave(parserType, pc);
	}

	public List<PC> getPCChildren() {
		ArrayList<PC> ret = new ArrayList<PC>();
		ret.add(pc);
		return ret;
	}

	public String getPCTypeName() {
		return pcTypeName;
	}

	public boolean isTokenParserConstructor() {
		return false;
	}	
	
}

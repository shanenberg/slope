/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope.lang.BIND;


import java.util.ArrayList;
import java.util.List;

import slope.PC;

public  class BINDPC<BOUNDED_OBJECT_TYPE> implements PC<BINDParser<BOUNDED_OBJECT_TYPE, PC<?>>> {

	final BOUNDED_OBJECT_TYPE boundedObject;
	final PC<?> childParser;

	public BINDPC(BOUNDED_OBJECT_TYPE boundedObject, PC<?> pc) {
		this.boundedObject = boundedObject;
		this.childParser = pc;
	}

	public BINDParser<BOUNDED_OBJECT_TYPE, PC<?>> createParser() {
		return new BINDParser<BOUNDED_OBJECT_TYPE, PC<?>>(boundedObject, childParser);
	}

	public List<PC> getPCChildren() {
		return new ArrayList<PC>();
	}

	public String getPCTypeName() {
		return "BIND";
	}


}

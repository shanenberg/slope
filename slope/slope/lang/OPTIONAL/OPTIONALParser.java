/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope.lang.OPTIONAL;

import slope.ElementParser;
import slope.PC;
import slope.Parser_1;
import slope.SetCmd;
import slope.TreeNode;
import slope.lang.NFOLD.NFOLDTreeNode;
import slope.lang.TOKEN.TokenParserConstructor;
import slope.lang.TOKEN.TokenTreeNode;
import slowscane.streams.TokenStream;

public class OPTIONALParser extends Parser_1<OPTIONALTreeNode, PC<?>> {
	
	public OPTIONALParser(PC pc) {
		super("OPTIONAL", pc);
	}

	public OPTIONALTreeNode parse(TokenStream<?> tokenStream) {
		
		int setBackPoint = tokenStream.currentPosition;

		try {
			ElementParser<? extends TreeNode> childParser = parserConstructor.createParser();
			TreeNode n = childParser.parse(tokenStream);
			return new OPTIONALTreeNode(n);
		} catch (Exception ex) {
			tokenStream.currentPosition = setBackPoint;
			return new OPTIONALTreeNode(null);
		}
	}
}

/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope.lang.SET;

import java.util.ArrayList;
import java.util.List;

import slope.PC;
import slope.SetCmd;
import slope.lang.TOKEN.TokenParserConstructor;

public class SetTokenValueParserConstructor<TOKEN_VALUE_TYPE> implements PC<SetTokenValueParser<TOKEN_VALUE_TYPE>> {
	public final TokenParserConstructor<TOKEN_VALUE_TYPE> tokenParserConstructor;
	public final SetCmd<TOKEN_VALUE_TYPE> so;
	
	public SetTokenValueParserConstructor(TokenParserConstructor<TOKEN_VALUE_TYPE> parserConstructor, SetCmd<TOKEN_VALUE_TYPE> so) {
		this.tokenParserConstructor = parserConstructor;
		this.so = so;
	}

	public SetTokenValueParser<TOKEN_VALUE_TYPE> createParser() {
		return new SetTokenValueParser<TOKEN_VALUE_TYPE>(tokenParserConstructor, so);
	}

	public List<PC> getPCChildren() {
		return new ArrayList<PC>();
	}

	public String getPCTypeName() {
		return "SETTOKENVALUE";
	}

}

/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope.lang.TOKEN;

import slope.ElementParser;
import slope.ParseException;
import slowscane.Token;
import slowscane.scanners.KeywordScanner;
import slowscane.scanners.TokenReader;
import slowscane.streams.TokenStream;

public  class TokenParser<TOKENTYPE> extends ElementParser<TokenTreeNode<TOKENTYPE>> {

	public final TokenReader<?> tokenReader;
	
	public TokenParser(TokenReader<TOKENTYPE> tokenReader) {
		super(null);
		this.tokenReader = tokenReader;
	}

	@Override
	public TokenTreeNode<TOKENTYPE> parse(TokenStream<?> tokenstream) {
//		System.err.println("XX: " + tokenReader);		
//		
//		if (tokenReader==null) {
//			new Exception().printStackTrace();
//			System.exit(-1);
//		}
//		
//		System.err.println("XX: " + tokenReader.toString());			
		try {

			@SuppressWarnings("unchecked")
			Token<TOKENTYPE> t = (Token<TOKENTYPE>) tokenReader.parseNext(tokenstream);
			TokenTreeNode<TOKENTYPE> ret = new TokenTreeNode<TOKENTYPE>(t);
//System.out.println("DONE");		
			return ret;
		} catch (Exception ex) {
//System.err.println("token pos" + tokenstream.currentPosition);	
//new Exception().printStackTrace();		
			
//System.out.println(tokenReader);
			
			throw new ParseException(
					tokenstream.stuckExceptionStringUpTo(tokenstream.currentPosition>=1?tokenstream.currentPosition - 1:0), 
					tokenReader.getClass().getName() + " cannot read token ");
		}
	}
}

/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package smos;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import shapuc.util.Reflection;
import slope.Parsing;
import slope.ResultObject;
import slope.lib.automata.ext.Automata;
import slope.lib.automata.ext.AutomataParsing;
import slope.lib.automata.ext.AutomataParsing.AutomataParser;

public class SMOSMachine<STATES, MESSAGES> {
	
	public SMOSMachine(String automataDefinition, Class stateEnumerationType, Class messageEnumerationType, Object controller) {
		this.controller = controller;
		
		this.stateEnumerationType = stateEnumerationType;
		this.messageEnumerationType = messageEnumerationType;
		createAutomataFromString(automataDefinition);
	}
	
	public final Class stateEnumerationType;
	public final Class messageEnumerationType;
	
	public final Object controller;
	
	public static final String emptyMessage = Automata.emptyMessage;	
	public static final String spontaneousMessage = Automata.spontaneousMessage;
	//  "ε"
	
	
	
	public STATES startState;
	public STATES currentState;
	public List<Transition<STATES, STATES>> transitions = 
			new ArrayList<Transition<STATES, STATES>>();
	
	
	public void invoke(MESSAGES inMessage, Context c) {
		List<Transition<STATES, STATES>> ts = getTransitions(currentState, inMessage);
		
		for (Transition<STATES, STATES> t: ts) {
			if (t.conditionHolds(c)) {
				t.invokeTransitionCmd(c);
				currentState = t.toState;
				t.invokeNextStateCmd(c);
				doSpontaneousTransitions(c);
				return;
			}
		}
		
		throw new RuntimeException("Invalid incoming message: " + inMessage);
		
	}
	
	private void doSpontaneousTransitions(Context c) {
		List<Transition<STATES, STATES>> ts = getTransitions(currentState);
		while (!ts.isEmpty()) {
			for (Transition<STATES, STATES> t : ts) {
				if (t.conditionHolds(c)) {

					t.invokeTransitionCmd(c);
					currentState = t.toState;
					t.invokeNextStateCmd(c);
					break;
				}
			}
			ts = getTransitions(currentState);
		}

	}

	public STATES currentState() {
		return currentState;
	}
	
	private List<Transition<STATES, STATES>> getTransitions(
			STATES currentState2,
			MESSAGES inMessage) {
		List<Transition<STATES, STATES>> ret = new ArrayList<Transition<STATES, STATES>>();
		
		for (Transition<STATES, STATES> transition : transitions) {
//			System.out.println("trans");
			if (transition.fromState.equals(currentState) && transition.message.equals(inMessage))
				ret.add(transition);
		}
		
		return ret;
	}
	
	private List<Transition<STATES, STATES>> getTransitions(
			STATES currentState2) {
		List<Transition<STATES, STATES>> ret = new ArrayList<Transition<STATES, STATES>>();
		
		for (Transition<STATES, STATES> transition : transitions) {
//			System.out.println("trans");
			if (transition.fromState.equals(currentState) && transition.isSpontaneous)
				ret.add(transition);
		}
		
		return ret;
	}	

	private void createAutomataFromString(String automataDefinition) {
		AutomataParsing tokens = new AutomataParsing();
		ResultObject<Automata> resultObject = new ResultObject<Automata>();
		@SuppressWarnings("rawtypes")
		Parsing p = new Parsing(tokens, automataDefinition); 
		p.parseWithParseTreeConstruction(tokens.new AutomataParser().START(resultObject.setter));
		createAutomataFromParseTree(resultObject.result);
		startAutomata();
	}
	
	private void startAutomata() {
		doSpontaneousTransitions(Context.emptyContext());
	}

	private void createAutomataFromParseTree(Automata a) {
		this.startState = getStateNamed(a.startStateName); 
		this.currentState = this.startState; 
		for (slope.lib.automata.ext.Transition t : a.transitions) {
			createTransition(t);
		}
	}
	
	private void createTransition(slope.lib.automata.ext.Transition t) {
		// TODO: The condition invoker needs to be written
		
		TransitionCondition transitionCondition = createTransitionCondition(t);
		AutomataCmd transitionCommand = createTransitionCommand(t);
		AutomataCmd transitionNextStateCommand = createNextStateCommand(t);
		
		Transition newTransition = 
				new Transition<STATES, MESSAGES>(
						t.isSpontaneous(), 
						t.isSpontaneous()? null : getMessageNamed(t.inMsg),
						getStateNamed(t.startStateName),
						getStateNamed(t.nextStateName),
						transitionCondition, transitionCommand, transitionNextStateCommand);
		transitions.add(newTransition);
	}
	
	private AutomataCmd createNextStateCommand(
			slope.lib.automata.ext.Transition t) {
		if (t.hasEmptyNextStateCommand()) return null;
		return AutomataCmd.createCommand(t.nextStateCmdName, controller);
	}

	private AutomataCmd createTransitionCommand(
			slope.lib.automata.ext.Transition t) {
		if (t.hasEmptyTransitionCommand()) return null;
		return AutomataCmd.createCommand(t.outMsg, controller);
	}

	private TransitionCondition createTransitionCondition(slope.lib.automata.ext.Transition t) {
		if (t.hasEmptyCondition())
			return null;
		if (t.hasNegatedCondition())
			return TransitionCondition.createNegatedCondition(t.conditionMsg, controller);;
		return TransitionCondition.createCondition(t.conditionMsg, controller);
	}
	
	private STATES getStateNamed(String stateName) {
//		System.out.println(stateName);
		try {
			return (STATES) getEnumerationFromString(stateName, stateEnumerationType);
		} catch (Exception e) {
			throw new SMOSException("Missing state: There is no state called " + stateName);
		}
	}
	
	private MESSAGES getMessageNamed(String messageName) {
		try {
			return (MESSAGES) getEnumerationFromString(messageName, messageEnumerationType);
		} catch (Exception e) {
			throw new SMOSException("Missing message: There is no message called " + messageName);
		}
	}	
	
	private Object getEnumerationFromString(String aString, Class enumClass) {
		Object[] enumerationConstrants = enumClass.getEnumConstants();
		for (int i = 0; i < enumerationConstrants.length; i++) {
			if (enumerationConstrants[i].toString().equals(aString))
				return enumerationConstrants[i];
		}
		throw new SMOSException("Missing message: There is no thing called " + aString);		
	}

}
